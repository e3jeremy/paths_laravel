@extends('layouts/master')
@section('content')
    <div class="dashboard">
        <router-view></router-view>
    </div>
@endsection

@push('laravel_to_vue_script')
    <script>
        window.App = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => Auth::user(),
            'signedIn' => Auth::check(),
            'role' => Auth::user()->getRoleNames()
        ]) !!};
    </script>
@endpush
