 <p>A customer has filled out the online form on our website. Here are the details:</p>
 <table>
   <tr>
    <td>First Name: </td><td>{{ $first_name }}</td>
   </tr>
   <tr>
    <td>Last Name: </td><td>{{ $last_name }}</td>
   </tr>
   <tr>
    <td>Email: </td><td>{{ $email }}</td>
   </tr>
   <tr>
    <td>Address: </td><td>{{ $address }}</td>
   </tr>
   <tr>
    <td>City: </td><td>{{ $city }}</td>
   </tr>
   <tr>
    <td>State/Province: </td><td>{{ $state }}</td>
   </tr>
   <tr>
    <td>Zip Code: </td><td>{{ $zip }}</td>
   </tr>
   <tr>
    <td>IP: </td><td>{{ $ip }}</td>
   </tr>
   <tr>
    <td>Last Visited URL: </td><td>{{ $last_visited }}</td>
   </tr>
    <tr>
    <td>Browser: </td><td>{{ $browser_info }}</td>
   </tr>
   <tr>
    <td>Session ID: </td><td>{{ $php_session }}</td>
   </tr>
   <tr>
    <td>Session: </td><td>{{ $session_json }}</td>
   </tr>
   <tr>
    <td>Pages:</td><td>{{ $page_path }}</td>
   </tr>
   <tr>
    <td>Category: </td><td>{{ $category_type }}</td>
   </tr>
   <tr>
    <td>Comments: </td><td>{{ $feedback }}</td>
   </tr>
   <tr>
    <td>Revenue Tracker: </td><td>{{ $addcode }}</td>
   </tr>
  <tr>
    <td>Email: </td><td>{{ $email }}</td>
   </tr>
 </table>
