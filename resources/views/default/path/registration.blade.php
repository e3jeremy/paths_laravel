@extends('templates/' . $options['themes']['template'] . '/main')

@section('content')
<form id="registration_form" method="post" name="hostedform" class="form-horizontal hostedform" enctype="text" action="{{ $action }}">

    <div class="card-body">
        @if($campaign_error)
        <div class="alert alert-danger" role="alert" style="border-left: 3px solid red">
            <h4 class="alert-heading">Error on submission.</h4>
            <p>@php
                echo htmlspecialchars_decode($campaign_error)
                @endphp
            </p>
            <hr />
            <p>Please fill up again.</p>
        </div>
        @endif
        <h5 style="margin-top: 10px;">
            Fill out  all the fields below and hit Submit to start the survey with PaidForResearch.com
        </h5>
        <hr />
        {{ csrf_field() }}
        <input type="hidden" name="submit" id="submit" value="eiq_registration">

        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>First Name</strong>
            </label>
            <div class="col-md-9">
                <input type="text" value="{{ urldecode($first_name) }}" name="first_name" class="form-control" placeholder="Enter First Name.." aria-required="true" required/>
                <span class="help-block text-info" style="font-size: 12px"></span>
                <div class="invalid-feedback" style="font-size: 12px">
                    Required
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>Last Name</strong>
            </label>
            <div class="col-md-9">
                <input type="text" value="{{ urldecode($last_name) }}" name="last_name" class="form-control" placeholder="Enter Last Name.." aria-required="true" required/>
                <span class="help-block text-info" style="font-size: 12px"></span>
                <div class="invalid-feedback" style="font-size: 12px">
                    Required
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>Email</strong>
            </label>
            <div class="col-md-9">
                <input type="email" value="{{ urldecode($email) }}" name="email" class="form-control" placeholder="Enter Email.." aria-required="true" required/>
                <span class="help-block text-info" style="font-size: 12px"></span>
                <div class="invalid-feedback" style="font-size: 12px">
                    Invalid Email Address.
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>Zip</strong>
            </label>
            <div class="col-md-9">
                <input type="text" value="{{ $zip }}" name="zip" class="form-control" pattern="[0-9]{5}" placeholder="Enter Zip Code.." aria-required="true" required/>
                <span class="help-block text-info" style="font-size: 12px">
                    Only US zip code are allowed.
                </span>
                <div class="invalid-feedback" style="font-size: 12px">
                    Please provide a valid US Zip Code
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>Birth Date</strong>
                <input name="birthdate" type="hidden" value="">
            </label>
            <div class="col-md-9">
                <div class="input-group">
                    <select name="dobmonth" class="form-control" required>
                        <!-- <option>MONTH</option> -->
                        <option value="" hidden>MONTH</option>
                		@for($i = 1; $i <= 12; $i++)
            			<option value="{{ str_pad($i, 2, 0, STR_PAD_LEFT) }}" {{ ($i == $dobmonth) ? 'selected': '' }}>
                            {{ str_pad($i, 2, 0, STR_PAD_LEFT) }}
                        </option>
                		@endfor
                    </select>
                    <div class="input-group-append">
                        <span class="input-group-text"> - </span>
                    </div>
                    <select name="dobday" class="form-control" required>
                        <option value="" hidden>DAY</option>
                		@for($i = 1; $i <= 31; $i++)
            			<option value="{{ str_pad($i, 2, 0, STR_PAD_LEFT) }}" {{ ($i == $dobday) ? 'selected': '' }}>
                            {{ str_pad($i, 2, 0, STR_PAD_LEFT) }}
                        </option>
                		@endfor
                    </select>
                    <div class="input-group-append">
                        <span class="input-group-text"> - </span>
                    </div>
                    <select name="dobyear" class="form-control" required>
                        <option value="" hidden>YEAR</option>
                		@for($i = date('Y'); $i >= date('Y', strtotime('-100 years')); $i--)
            			<option value="{{ $i }}" {{ ($i == $dobyear) ? 'selected': '' }}>
                            {{ $i }}
                        </option>
                		@endfor
                    </select>
                </div>
                <div class="invalid-feedback" style="font-size: 12px"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                <strong>Gender</strong>
            </label>
            <div class="col-md-9">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <input name="gender" value="M" type="radio" class=""  {{ (strtolower($gender) == 'm') ? 'checked': '' }} required/>
                        </span>
                    </div>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            MALE
                        </span>
                    </div>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <input name="gender" value="F"  type="radio" class=""  {{ (strtolower($gender) == 'f') ? 'checked': '' }} required/>
                        </span>
                    </div>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            FEMALE
                        </span>
                    </div>
                </div>
                <span class="help-block text-info" style="font-size: 12px"></span>
                <div class="invalid-feedback" style="font-size: 12px"></div>
            </div>
        </div>
        <hr />
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-uppercase" for="">
                &nbsp;
            </label>
            <div class="col-md-9">
                <div id="agree_first" class="alert alert-danger" role="alert" style="display: none; border-left: 3px solid red">
                    <h4 class="alert-heading">Please Agree First!</h4>
                    <p>We need you to read first our terms of use and privacy policy.</p>
                    <hr />
                    <p>Then click on checkbox if you agree.</p>
                </div>
                <div id="fix_first" class="alert alert-danger" role="alert" style="display: none; border-left: 3px solid red">
                    <p>Please fix first the error on zip code.</p>
                </div>
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <input type="checkbox" name="agree" class="" value="{{ $agree_code }}"   {{ ($agree != 'off') ? 'checked': '' }} required/>
                        </span>
                    </div>
                    <div class="input-group-prepend">
                        <label class="form-check-label pl-4" style="xwhite-space: pre-line;">I agree to the
                            <a href="#" data-toggle="modal" data-target="#terms">Terms of Use</a> &amp;
                            <a href="#" data-toggle="modal" data-target="#privacy">Privacy Policy</a>
                            <br />
                            and to receive daily email from <strong>PaidForResearch.com and Research Unlimited Inc.</strong></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button id="disabled_btn" class="btn btn-lg btn-secondary"><i class="fa fa-dot-circle-o"></i> Submit</button>
        <button id="enabled_btn" type="submit" class="btn btn-lg btn-primary" style="display: none;"><i class="fa fa-dot-circle-o"></i> Submit</button>
    </div>
</form>
@endsection

@push('script')
<script type="text/javascript">
    $(function() {

        /**
         * Action on agreeing the rterms and conditions
         * Enable/disable submit button
         */
        $('input[name="agree"]').on('click', function(e) {
            if ($(this).is(':checked')) {
                if ($('input[name="zip"]').hasClass('is-invalid')) {
                    $(this).trigger('click');
                    $('#fix_first').show();
                    $('input[name="zip"]').parent().find('.invalid-feedback').html('Please provide a valid US Zip Code.<br /> Before you check the "Terms of Use" checkbox.').show();
                    return;
                }
                $('#agree_first, #disabled_btn, #fix_first').hide();
                $('#enabled_btn').show();
            } else {
                $('#enabled_btn, #fix_first').hide();
                $('#disabled_btn').show();
            }
        });
        /**
         * Action when disabled submit button was click
         */
        $('#disabled_btn, #enabled_btn').on('click', function() {
            console.log($('input[name="agree"]').is(':checked'));
            if (!$('input[name="agree"]').is(':checked')) $('#agree_first').show();
        });

        /**
         * Action when submit button is click
         * Show Error message if has error
         */
        $('button[type="submit"]').on('click', function() {
            $('input, select').each(function() {

                if($(this).val() == '') {
                    $(this).removeClass('is-valid').addClass('is-invalid');
                    $(this).parent().find('.invalid-feedback').text('This is a required field.').show();
                    $(this).parents('.col-md-9').find('.invalid-feedback').text('This is a required field.').show();
                } else {
                    $(this).removeClass('is-invalid').addClass('is-valid');
                    if($(this).attr('name') != 'email') {
                        $(this).parent().find('.invalid-feedback').hide();
                    } else {
                        var dobmonth = $('select[name="dobmonth"]').val();
                        var dobday = $('select[name="dobday"]').val();
                        var dobyear = $('select[name="dobyear"]').val();
                        if(dobmonth && dobday && dobyear) $(this).parents('.col-md-9').find('.invalid-feedback').hide();
                    }
                }

                if($(this).attr('name') == 'gender') {
                     if($('input[name="gender"]').is(':checked')) {
                        $(this).parent().removeClass('is-invalid').addClass('is-valid');
                        $(this).parents('.col-md-9').find('.invalid-feedback').hide();
                    } else {
                       $(this).parent().removeClass('is-valid').addClass('is-invalid');
                       $(this).parents('.col-md-9').find('.invalid-feedback').text('This is a required field.').show();
                    }
                }
            });
        });

        /**
         * Action When input is change or blur on input type text and email
         * Show Error message if has error
         * Filter email for correct format
         */
        $('input[type="text"], input[type="email"]').on('change, blur', function () {
            if($(this).val()) {
                if($(this).attr('type') != 'email') {
                    $(this).removeClass('is-invalid').addClass('is-valid');
                    $(this).parent().find('.invalid-feedback').hide();
                } else {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    if(filter.test($(this).val())) {
                        $(this).removeClass('is-invalid').addClass('is-valid');
                        $(this).parent().find('.invalid-feedback').hide();
                    }
                    else {
                        $(this).removeClass('is-valid').addClass('is-invalid');
                        $(this).parent().find('.invalid-feedback').text('Invalid email address.').show();
                    }
                }
            }
            else {
                $(this).removeClass('is-valid').addClass('is-invalid');
                $(this).parent().find('.invalid-feedback').text('This is a required field.').show();

            }
        });

        /**
         * Action when zip value was change
         * Make an api call to check the value is a correct US zip code
         * Show Error message if has error
         */

        function zipChecker (input)
        {
            if($('input[name="agree"]').is(':checked')) $('input[name="agree"]').trigger('click');

            console.log('zip bind: ', input.val());
            $.ajax({
                url: 'http://leadreactor.engageiq.com/zip_checker',
                dataType: 'jsonp',
                data: {
                    zip: input.val()
                }
            }).done(function($data) {
                if($data !== 'true') {
                    if($('input[name="agree"]').is(':checked')) $('input[name="agree"]').trigger('click');

                    input.removeClass('is-valid').addClass('is-invalid');
                    input.parent().find('.invalid-feedback').text('Please provide a valid US Zip Code.').show();
                }
                else {
                    input.removeClass('is-invalid').addClass('is-valid');
                    input.parent().find('.invalid-feedback').hide();
                    $('#fix_first').hide();
                }
            });
        }
        $('input[name="zip"]')
        .bind('input', function () {
            zipChecker($(this));
        })
        .on('change, focusout, blur', function(){
            zipChecker($(this));
        });

        /**
         * Action when selection for birthdate was change
         * Show Error message if has error
         */
        $('select').on('change, focusout', function () {
            if(!$('select[name="' + $(this).attr('name') + '"]').val()) {
                $(this).removeClass('is-valid').addClass('is-invalid');
                $(this).parents('.col-md-9').find('.invalid-feedback').text('This is a required field.').show();
            }
        }).on('mouseleave', function () {
            if($('select[name="' + $(this).attr('name') + '"]').val()) {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
            var dobmonth = $('select[name="dobmonth"]').val();
            var dobday = $('select[name="dobday"]').val();
            var dobyear = $('select[name="dobyear"]').val();
            if(dobmonth && dobday && dobyear){
                $('input[name="birthdate"]').val(dobyear + '-' + dobmonth + '-' + dobday);
                $(this).parents('.col-md-9').find('.invalid-feedback').hide();
            }
        });
        /**
         * Action when selection for gender is click
         * Hide Error message
         */
        $('input[name="gender"]').on('click', function() {
            $('input[name="gender"]').each(function () {
                $(this).parent().removeClass('is-invalid').addClass('is-valid');
            });
            $(this).parents('.col-md-9').find('.invalid-feedback').hide();
        });
    });
</script>
@endpush
