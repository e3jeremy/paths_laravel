@extends('templates/' . $options['themes']['template'] . '/main')

@section('content')
<div class="card-body">
    @if($token_error)
    <div class="alert alert-danger" role="alert" style="border-left: 3px solid red">
        <h4 class="alert-heading">Can't Connect to Lead Reactor</h4>
        <p>Error: {{ $token_error }}</p>
        <hr />
        <p>Please try/refresh again the page, the page were having a problem in connecting to api client.</p>
    </div>
    @else
    <div style="text-align:center;">
        <span class="title_text" style="font-size:20px; color: #565656;">
            Accepting
            <span style="color: #4d8478; border: 2px solid #10aa85; padding: 1px 2px 1px 6px; margin-right: 5px;">
                {{ $accepting }}
            </span>
            survey takers as of
            <span id="engage_clock1" style="color: #4d8478;">
                {{ Carbon\Carbon::now()->format('Y g:i:s') }}:00 {{ Carbon\Carbon::now()->format('A') }}
            </span>
        </span>
        <br>
    </div>
    <br>
    <div style="text-align:center;">
        <p style="font-size: 23px; color: #565656; margin-top: 10px;">
            Do you want to begin your survey<br> and start earning?
        </p>
        <div style="text-align: center;">
                <button type="button" id="start_survey" class="btn btn-outline-primary btn-lg">Start Survey</button>
        </div>
        <div style="clear:both"></div>
    </div>
    @endif
</div>
@endsection

@push('script')
<script>
    function checkTime(i){if (i<10){i="0" + i;}return i;}

    function display_time() {
        var today=new Date();
        var Y=today.getYear();
        if (Y< 1000) Y+=1900;
        var M=today.getMonth();
        var d=today.getDate();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        var SS=today.getMilliseconds();
        var MM="AM";

        if(h>12){ h=h-12; MM="PM"; }
        // add a zero in front of numbers<10
        m=checkTime(m);
        s=checkTime(s);
        SS=checkTime(Math.round(SS/10));
        $('#engage_clock1').html(Y+" "+h+":"+m+":"+s+":"+SS+" "+MM);
    }

    $(function() {
        var myVar = setInterval(function(){display_time()},10);

        $('#start_survey').on('click', function() {
            window.location.href = '/paths/{{ $slug }}/registration';
        });
    });
</script>
@endpush
