@extends('templates/' . $options['themes']['template'] . '/main')

@section('content')
<form id="registration_form" method="post" name="hostedform" class="form-horizontal hostedform" enctype="text" action="">
    {{ csrf_field() }}
    <input type="hidden" name="first_name" value="{{ $first_name }}">
    <input type="hidden" name="last_name" value="{{ $last_name }}">
    <input type="hidden" name="email" value="{{ $email }}">
    <input type="hidden" name="zip" value="{{ $zip }}">
    <input type="hidden" name="birthdate" value="{{ $birthdate }}">
    <input type="hidden" name="dobmonth" value="{{ $dobmonth }}">
    <input type="hidden" name="dobday" value="{{ $dobday }}">
    <input type="hidden" name="dobyear" value="{{ $dobyear }}">
    <input type="hidden" name="gender" value="{{ $gender }}">
    <div class="card-body">

        @if($campaign_error)
        <div class="alert alert-danger" role="alert" style="border-left: 3px solid red">
            <h4 class="alert-heading">Error on submission.</h4>
            <p>@php
                echo htmlspecialchars_decode($campaign_error)
                @endphp
            </p>
            <hr />
            <p>Please choose again.</p>
        </div>
        @endif
        <input type="hidden" name="submit" id="submit" value="eiq_question">
        <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12" style="margin-left: auto; margin-right: auto">
            @php
                foreach($questions as $id => $question):
                $filters[] = 'filter_questions['.$id.']';
            @endphp
            <div id="contentbox">
                <div align="center">
                    <table>
                        <tbody>
                            <tr>
                                <td class="offer-column-1">
                                    <div class="new-yesno-button">
                                        <input id="filter_question_{{ $id }}_yes" name="filter_questions[{{ $id }}]" type="radio" value="YES">
                                        <label class="class-yes" for="filter_question_{{ $id }}_yes">Yes</label>
                                        <input id="filter_question_{{ $id }}_no" name="filter_questions[{{ $id }}]" type="radio" value="NO">
                                        <label class="class-no" for="filter_question_{{ $id }}_no">No</label>
                                    </div>
                                </td>
                                <td class="offer-column-2">
                                    <div class="content-desktop2">
                                        {{ $question }}<span style="color:red">*</span>
                                    </div>
                                </td>
                                <td class="offer-column-3">
                                    @php
                                        $profile_icon = '/img/default/default_profile_icon.png';
                                        if(array_key_exists($id, $icons)) $profile_icon = ($icons[$id]) ? $icons[$id] : $profile_icon;
                                    @endphp
                                    <img class="m-badge" src="{{ $profile_icon }}" align="absmiddle">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="separator" style=""></div>
            @php
                endforeach;
            @endphp
            <div align="center"><button id="submit_question_button" type="submit" class="submit_button_form" align="absmiddle">Submit</button></div>
        </div>
    </div>
</form>
@endsection

@push('script')
    <script>
    </script>
@endpush
