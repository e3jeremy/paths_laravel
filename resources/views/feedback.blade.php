<!DOCTYPE html>
<html lang="en">
<head>
	<title>Feedback</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>

<body class="app header-fixed aside-menu-fixed aside-menu-hidden">
	<div class="app-body mb-5">
		<main role="main" class="main">

			<div class="container  mt-5">
				@if($error)
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-danger" style="border-left: 3px solid red;">
							<p>{{ $error }}</p>
						</div>
					</div>
				</div>
				@endif
				<div class="card">
					<div class="card-body">
						<div id="cform">
							<div class="container-fluid">
								<h2>Report any errors here.</h2>
								<!-- FEEDBACK FORM -->
								<form id="feedback-form" method="post" action="" role="form" class="form-horizontal" enctype="multipart/form-data">
							        {{ csrf_field() }}
									<div class="form-group">
										<div class="col-sm-12">
											<p style="display:{{ ($first_name && $zip_status != 'invalid') ? 'none': 'block' }}">
												<label>First Name</label><br>
												<input type="text" name="first_name" id="first_name" value="{{ $first_name }}" class="form-control" required>
											</p>
											<p style="display:{{ ($last_name && $zip_status != 'invalid') ? 'none': 'block' }}">
												<label>Last Name</label><br>
												<input type="text" name="last_name" id="last_name" value="{{ $last_name }}" class="form-control" required>
											</p>
											<p style="display:{{ ($email && $zip_status != 'invalid') ? 'none': 'block' }}">
												<label>Email</label><br>
												<input type="email" name="email" id="email" value="{{ $email }}" class="form-control" required>
											</p>
											<p style="display:{{ ($zip && $zip_status != 'invalid') ? 'none': 'block' }}">
												<label>Zip</label><br>
												<input type="text" name="zip" id="zip" value="{{ $zip }}" class="form-control {{ ($zip_status == 'invalid') ? 'is-invalid': '' }}" pattern="[0-9]{5}" required>
												<span class="help-block text-info" style="font-size: 12px">
													Only US zip code are allowed.
												</span>
												<span class="invalid-feedback" style="font-size: 12px; display:{{ ($zip_status != 'invalid') ? 'none': 'block' }}">
													Please provide a valid US Zip Code
												</span>
											</p>
											<p>
												<label>Topic</label><br>
												<select name='input-comments' id='input-comments' class="form-control" required>
													<option value=""></option>
													<option value="Support" {{ ($category_type == 'Support') ? 'selected' : '' }}>Support</option>
													<option value="Questions"  {{ ($category_type == 'Questions') ? 'selected' : '' }}>Questions</option>
													<option value="Technical Problem"  {{ ($category_type == 'Technical Problem') ? 'selected' : '' }}>Technical Problem</option>
													<option value="Website not loading"  {{ ($category_type == 'Website not loading') ? 'selected' : '' }}>Website not loading</option>
												</select>
											</p>
											<br>
											<div class="">
												<div class="to-be-hidden alert alert-danger" style="display: none; border-left: 3px solid red;">
													<h4>Call Us !</h4>
													<p>If you call and report the problem, you will get <strong>$5</strong>.</p>
													<p>Please call <strong>1-800-394-7713</strong> Mon - Fri 8:00AM to 5PM PST.</p>
													<hr />
													<!-- OLD NUMBER 1-800-941-9004 -->
													<p>You will not receive a sales pitch. We want to keep the experience good for you.<br />
													This feedback will help us improve. You may also qualify to receive <strong>$25.</strong> Call to find out how.</p>
												</div>
											</div>
											<span>
												<strong>Enter your feedback</strong>
											</span>
											<br>
											<textarea name="feedback" class="form-control" rows="4" id="input-comments-content" required>{{ $feedback }}</textarea>
										</div>
									</div>
									<!-- SUBMIT BUTTON -->
									<div class="form-group">
										<div class="col-sm-10"><!-- col-sm-offset-2 -->
											<input type="submit" id="submitbtn" value="Send Feedback" class="btn btn-primary">
										</div>
									</div>
									<input type="hidden" name="zip_status" id="zip_status" value="">
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
		</main>
	</div>
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script type="text/javascript">
		/**
		 * Action when zip value was change
		 * Make an api call to check the value is a correct US zip code
		 * Show Error message if has error
		 */

		function zipChecker (input)
		{
			if($('input[name="agree"]').is(':checked')) $('input[name="agree"]').trigger('click');

			console.log('zip bind: ', input.val());

			$.ajax({
				url: 'http://leadreactor.engageiq.com/zip_checker',
				dataType: 'jsonp',
				data: {
					zip: input.val()
				}
			}).done(function($data) {
				if($data !== 'true') {
					if($('input[name="agree"]').is(':checked')) $('input[name="agree"]').trigger('click');

					input.removeClass('is-valid').addClass('is-invalid');
					input.parent().find('.invalid-feedback').text('Please provide a valid US Zip Code.').show();
					$('#zip_status').val('invalid');
				}
				else {
					input.removeClass('is-invalid').addClass('is-valid');
					input.parent().find('.invalid-feedback').hide();
					$('#fix_first').hide();
					$('#zip_status').val('');
				}
			});
		}
		$(document).ready(function()
		{
			$('.to-be-hidden').hide();

			$('#input-comments').change(function() {
				var selected = $('#input-comments option:selected').text();
				var toBeHidden = $('.to-be-hidden');

				if(selected === 'Website not loading' || selected === 'Technical Problem') toBeHidden.show();
				else toBeHidden.hide();
			});

			// $("#feedback-form").validate({
			// 	submitHandler: function(form) {
			// 		$('#submitbtn').val('SENDING').attr('type','button').prop('disabled',true);
			// 		form.submit();
			// 	}
			// });

	        $('input[name="zip"]')
	        .bind('input', function () {
	            zipChecker($(this));
	        })
	        .on('change, focusout, blur', function(){
	            zipChecker($(this));
	        });
		});
	</script>
</body>
</html>
