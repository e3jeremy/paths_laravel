<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Dashboard</title>
        <link href="{{ mix("css/app.css") }}" rel="stylesheet"/>
        <link href="/css/style.css" rel="stylesheet">
        <meta name="Paid For Research" value="notranslate">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden footer-fixed">
        <div id="dashboard">
            <customheader>
                @yield('customheader')
            </customheader>
            <div class="app-body">
                <sidebar>
                    @yield('appbody')
                </sidebar>
                <div class="main">
                    <appbody>
                        @yield('appbody')
                    </appbody>
                    <div class="container-fluid">
                        <div class="animated fadeIn">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
            <appfooter>
                @yield('appfooter')
            </appfooter>
        </div>
        <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="/js/main.js"></script>
        @stack('laravel_to_vue_script')
        <script src="{{ mix("js/app.js") }}"></script>
    </body>
</html>
