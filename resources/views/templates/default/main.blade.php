<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="{{ $options['general']['meta_description'] }}">
		<meta name="author" content="{{ $options['general']['meta_author'] }}">
		<meta name="keywords" content="{{ $options['general']['meta_keywords'] }}">
		<!-- <link rel="icon" href="../../../../favicon.ico"> -->

		<title>{{ $options['general']['path_title'] }}</title>

		<!-- Bootstrap core CSS -->
		<!-- <link href="/css/color/{{ $options['themes']['color'] }}.css" rel="stylesheet"> -->
		@yield('stylesheet')
		<script src="/js/pathhead.js"></script>
		<!-- <script src="/js/jquery.validate.min.js"></script> -->
	</head>

	<body class="
		app header-fixed
		aside-menu-fixed
		aside-menu-hidden
		page-{{ $current_page }}
		template-{{ $options['themes']['template']}}
		template-color-{{ $options['themes']['color']}}
	" style="
		@if($options['general']['body_background_img'])
		background-image: url('{{ $options['general']['body_background_img'] }}');
		background-position: {{ $options['general']['body_background_img_position'] }};
		background-repeat: {{ $options['general']['body_background_img_repeat'] }};
		background-size: auto auto;
		@endif
	">
		@if($options['nav_bar']['show_nav_bar'] === 'true')
		<header class="app-header navbar">
			@section('header-nav-bar')
				<a href="#" class="navbar-brand">
					@if($options['nav_bar']['nav_bar_logo'])
					<img src="{{ url($options['nav_bar']['nav_bar_logo']) }}" alt="" style="height: 35px; margin: 0 10px;">
					@endif
					<span>{{ $options['general']['path_title'] }}</span>
				</a>
			@show
		</header>
		@endif
		<div class="app-body mb-5" style="{{ (!$options['nav_bar']['show_nav_bar'] === 'true') ? 'margin-top: 0;': '' }}">
			<main role="main" class="main">
				@if($options['header']['show_header'] === 'true')
				<div class="
					jumbotron
					d-flex
					@if($options['header']['show_background_color'] === 'true')
					bg-primary
					@endif
					align-items-center
					text-white-50
					text-white box-shadow
				" style="
					@if($options['header']['show_background_color'] !== 'true')
					background-color: transparent;
					@endif
					@if($options['header']['header_background_img'])
					background-image: url('{{ $options['header']['header_background_img'] }}');
					background-position: {{ $options['header']['header_background_img_position'] }};
					background-repeat: {{ $options['header']['header_background_img_repeat'] }};
					background-size: auto auto;
					@endif
				">
				    <div class="container">
						@if($options['header']['show_header_text'] === 'true')
				        <h1 class="display-3">{{ $options['header']['header_title'] }}</h1>
				        <p>{{ $options['header']['header_sub_title'] }}</p>
						@else
						<h1 class="display-3">&nbsp;</h1>
				        <p>&nbsp;</p>
						@endif
				    </div>
				</div>
				@endif
				<div class="container" style="{{ (!$options['header']['show_header'] === 'true') ? 'margin-top: 50px;': '' }}">
					<div class="
						card
					" style="
						@if($options['content']['content_background_img'])
						background-image: url('{{ $options['content']['content_background_img'] }}');
						background-position: {{ $options['content']['content_background_img_position'] }};
						background-repeat: {{ $options['content']['content_background_img_repeat'] }};
						background-size: auto auto;
						@endif
					">
					    <div class="card-header bg-white">
							@if($is_stack_page)
							<h4 class="my-4" style="font-weight: 500;">
								<center>
									Based off of your provided answers, here are the top offers just for you!
								</center>
							</h4>
							@endif
					        @if($options['progress_bar']['show_progress_bar'] === 'true')
								@if($options['progress_bar']['progress_bar_type'] == 'custom')
								@include('progressbar.' . $options['progress_bar']['custom_file_name'])
								@else
						        <div class="
									progress
								" style="
									height: {{ $options['progress_bar']['progress_bar_height'] }}px;
								">
						            <div class="{{ $options['progress_bar']['progress_bar_type'] }} bg-primary"
										 role="progressbar"
										 aria-valuenow="{{ $progress }}"
										 aria-valuemin="0"
										 aria-valuemax="100"
										 style="
										 width: {{ $progress }};
									 ">
									 @if($options['progress_bar']['progress_bar_type'] == 'progress-bar progress-bar-label')
									 	{{ $progress }}
									 @endif
									 </div>
						        </div>
					        	@endif
					        @else
					            &nbsp;
					        @endif
							@if($is_stack_page)
							<div class="mt-3" style="font-size: 16px;">
								<center>
									<span>
										<span class=" text-primary font-weight-bold">{{ $current_stack }}</span> of
										<span class=" text-primary font-weight-bold">{{ $stack_count }}</span>
										<small class="text-muted"> Question(s) to go</small>
									</span>
								</center>
							</div>
							@endif
					    </div>
				        @yield('content')
					</div>

				</div>
			</main>
		</div>

		@if($options['footer']['show_footer'] === 'true')
		<footer class="app-footer" style="margin-left: 0; z-index: 999999; position: relative;">

			<div style="z-index: 999" class="slider closed" id="slider">
				<iframe src="/paths/{{ $slug }}/feedback/?current_page={{ $current_page }}" style="height: 790px; width: 100%; border: none;"></iframe>
				<div style="text-transform: uppercase; left: 50%; text-align: center; padding: 15px;">
					<span class="feedback_togler" style="cursor: pointer;"><i class="icon-close">Close</i></span>
				</div>
			</div>
		    <span style="display: inline-flex;">
		        <a href="#">{{ $options['general']['path_title'] }} </a> © <?php echo date("Y") ?></span>
			<span style="display: inline-flex; margin: 0 auto;">
					@if($options['feedback']['show_feedback'] == 'true')
			        <a class="feedback_togler" href="javascript:void(0)"> Feedback: Report any errors here. </a> |
					@endif
			        <a href="#" data-toggle="modal" data-target="#privacy"> Privacy Policy </a> |
					<a href="#" data-toggle="modal" data-target="#terms"> Terms of Services</a><br>
			</span>
		    <span style="display: inline-flex;">
		        Powered by <a href="http://engageiq.com"> EngageIQ</a>
		    </span>
		</footer>
		@endif

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->

		<script src="/js/path.js"></script>
		<script>
			$(function() {
				$('.feedback_togler').on('click', function () {
					document.getElementById('slider').classList.toggle('closed');
				});
			});
		</script>
		@if(is_array($options['trackers']['files']))
			@foreach($options['trackers']['files'] as $file)
				@include('trackers.' . $file)
			@endforeach
		@endif
		@stack('script')

		@include('includes.modal.privacy')
		@include('includes.modal.terms')
	</body>
</html>
