@extends('default.path.landing')

@section('stylesheet')
<link href="/css/paths/dynamic-live.min.css?{{ $options['style']['version'] }}" rel="stylesheet">
@endsection
