@extends('default.path.campaign')

@section('stylesheet')
<link href="/css/paths/dynamic-live.min.css?{{ $options['style']['version'] }}" rel="stylesheet">
@endsection
