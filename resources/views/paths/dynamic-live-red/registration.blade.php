@extends('default.path.registration')

@section('stylesheet')
<link href="/css/paths/dynamic-live-red.min.css?{{ $options['style']['version'] }}" rel="stylesheet">
@endsection
