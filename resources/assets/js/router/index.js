import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../components/Dashboard'
import Paths from '../components/Paths'
import Users from '../components/Users'
import Roles from '../components/Roles'
import UserProfile from '../components/UserProfile'
import Bugs from '../components/Bugs'
import ReportBug from '../components/ReportBug'
import BugProfile from '../components/BugProfile'
import ProjectDocumentation from '../components/ProjectDocumentation'
import Error500 from '../components/Error500'
import Error404 from '../components/Error404'

Vue.use(Router)

export default new Router({
	mode: 'history',
  	routes: [
	    {
	      	path: '/admin',
	      	name: 'Dashboard',
	      	component: Dashboard
	    },
		{
			path: '/admin/paths',
			name: 'Paths',
			component: Paths
		},
        {
            path: '/admin/users',
            name: 'Users',
            component: Users
        },
		{
			path: '/admin/UserProfile/:userId',
			name: 'userProfile',
			component: UserProfile,
			props: true
		},
        {
            path: '/admin/roles',
            name: 'Roles',
            component: Roles
        },
		{
			path: '/admin/bugs',
			name: 'Bugs',
			component: Bugs
		},
		{
			path: '/admin/report-bugs-or-comment',
			name: 'ReportBug',
			component: ReportBug
		},
		{
			path: '/admin/bug-profile/:id',
			name: 'BugProfile',
			component: BugProfile,
			props: true
		},
		{
			path:'/admin/project-documentation',
			name: 'ProjectDocumentation',
			component: ProjectDocumentation
		},
		{
			path: '/admin/error-500',
			name: 'Error500',
			component: Error500
		},
		{
			path: '/admin/error-404',
			name: 'Error404',
			component: Error404
		}
  	]
})
