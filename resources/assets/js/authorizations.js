let user = window.App.user;

module.exports = {
    owns (model, prop = 'created_by') {
        return (model[prop]) === user.name;
    },
    //dont update your own
    userOwner(model, prop = 'id') {
        return (model[prop] === user.id)
    }
};
