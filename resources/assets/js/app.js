
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


require('popper.js');
require('tooltip.js');
require('bootstrap/dist/js/bootstrap.js');
require('pace/pace.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import router from './router';

import customheader from './partials/Header.vue';
import sidebar from './partials/Sidebar.vue';
import appbody from './partials/Appbody.vue';
import appfooter from './partials/Footer.vue';

import Toaster  from 'v-toaster';
import 'v-toaster/dist/v-toaster.css';
Vue.use(Toaster, {timeout: 5000});

import VModal from 'vue-js-modal';
Vue.use(VModal)

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

import Spinners from './components/spinners';
Vue.component('spinner', Spinners);

Vue.component("wysiwyg", require("./components/Wysiwyg.vue"));

const app = new Vue({
    el: '#dashboard',
    components : {
        customheader,
        sidebar,
        appbody,
        appfooter,
    },
    router
});
