/*
Navicat SQLite Data Transfer

Source Server         : sqlite_homestead
Source Server Version : 30623
Source Host           : localhost:0

Target Server Type    : SQLite
Target Server Version : 30623
File Encoding         : 65001

Date: 2018-02-20 16:25:27
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."users"
-- ----------------------------
DROP TABLE "main"."users";
CREATE TABLE "users" ("id" integer not null primary key autoincrement, "name" varchar not null, "email" varchar not null, "avatar" text null, "role" varchar null, "last_loggedin" datetime null, "status" tinyint(1) not null default '1', "created_by" varchar null, "updated_by" varchar null, "password" varchar not null, "remember_token" varchar null, "created_at" datetime null, "updated_at" datetime null);

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO ""."users" VALUES (1, 'ian', 'ianrussel@engageiq.com', null, null, '2018-02-20 07:09:22', 1, null, null, '$2y$10$e2UqR0Ep1r6FEN5/GcBiHu9YRNLmr2btYkGkT817c35bWfPvWMN3q', 'wAwssFOUw4zlXFru0kXZGmLJDfAKzDJJq4XHuujrIAXIoDxw8OLQsN8EPH6W', '2018-02-19 05:57:58', '2018-02-20 07:09:22');
