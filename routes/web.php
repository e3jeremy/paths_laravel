<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
*/
Auth::routes();

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
| All routes here needs authentication
*/
Route::get('/', function() {
    return redirect('/admin');
});
Route::get('/admin/{vue?}/{slug1?}', 'DashboardController@index');

/*
|--------------------------------------------------------------------------
| Login/ Logout Routes
|--------------------------------------------------------------------------
*/
Route::get('/logout', function() {
    Auth::logout();
    return redirect('/login');
});

/**
 *  Email Confirmation Routes
 */
Route::get('/register/confirm', 'Auth\RegisterConfirmationController@index');
Route::get('/no-confirmation-token', function() {
    return "Your not a robot, right?";
});

/*
|--------------------------------------------------------------------------
| Common Path Routes
|--------------------------------------------------------------------------
*/
Route::get('/preview/{template}', 'TemplateController@preview');
Route::get('/preview/{template}/{color}', 'TemplateController@preview');
Route::get('/paths/{slug}/feedback', 'FeedbackController@feedback');
Route::post('/paths/{slug}/feedback', 'FeedbackController@processFeedback');
Route::get('/path-image/{slug}/{image}', 'PathController@previewImage');

/*
|--------------------------------------------------------------------------
| Default Path Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
*/

Route::any('/paths/session/{action}', 'Paths\DefaultPathController@session');
Route::get('/paths/default', 'Paths\DefaultPathController@landing')->name('default-landing')->middleware('nlrToken');
Route::get('/paths/default/registration', 'Paths\DefaultPathController@registration')->name('default-registration');
Route::post('/paths/default/registration', 'Paths\DefaultPathController@processRegistration');
Route::any('/paths/default/question', 'Paths\DefaultPathController@question')->name('default-question');
Route::post('/paths/default/question', 'Paths\DefaultPathController@processQuestion');
Route::get('/paths/default/campaign/{page}', 'Paths\DefaultPathController@campaign')->name('default-campaign');

/*
|--------------------------------------------------------------------------
| Test
|--------------------------------------------------------------------------
*/
Route::get('/test', function() {
    dd();
});
//test your email tempolate here
Route::get('/mailable', function() {
    $user = App\User::latest('id')->first();
    //dd($user);
    $password = '1234567';
    return new App\Mail\UserCreated($user,$password);
});
// Dynamic method
Route::get('/test/{method}', 'TestController@index');
