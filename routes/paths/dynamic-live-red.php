<?php

/*
 | ---------------------------------------------------------------------------------------------------------------------------------
 | Generated routes
 | ---------------------------------------------------------------------------------------------------------------------------------
 |
 */

 Route::get('/dynamic-live-red', 'Paths\DynamicLiveRedController@landing')->name('dynamic-live-red-landing')->middleware('nlrToken');
 Route::get('/dynamic-live-red/registration', 'Paths\DynamicLiveRedController@registration')->name('dynamic-live-red-registration');
 Route::post('/dynamic-live-red/registration', 'Paths\DynamicLiveRedController@processRegistration');
 Route::get('/dynamic-live-red/question', 'Paths\DynamicLiveRedController@question')->name('dynamic-live-red-question');
 Route::post('/dynamic-live-red/question', 'Paths\DynamicLiveRedController@processQuestion');
 Route::get('/dynamic-live-red/campaign/{page}', 'Paths\DynamicLiveRedController@campaign')->name('dynamic-live-red-campaign');
