<?php

/*
 | ---------------------------------------------------------------------------------------------------------------------------------
 | Generated routes
 | ---------------------------------------------------------------------------------------------------------------------------------
 |
 */

 Route::get('/dynamic-live', 'Paths\DynamicLiveController@landing')->name('dynamic-live-landing')->middleware('nlrToken');
 Route::get('/dynamic-live/registration', 'Paths\DynamicLiveController@registration')->name('dynamic-live-registration');
 Route::post('/dynamic-live/registration', 'Paths\DynamicLiveController@processRegistration');
 Route::get('/dynamic-live/question', 'Paths\DynamicLiveController@question')->name('dynamic-live-question');
 Route::post('/dynamic-live/question', 'Paths\DynamicLiveController@processQuestion');
 Route::get('/dynamic-live/campaign/{page}', 'Paths\DynamicLiveController@campaign')->name('dynamic-live-campaign');
