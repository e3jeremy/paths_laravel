<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PleaseConfirmYourEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user associated with the email.
     *
     * @var \App\User
     */
    public $user;
    public $password;

    /**
     * Create a new mailable instance.
     *
     * @param \App\User $user
     */
    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the email.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails/users.confirm-email')->with('password', ['password' => $this->password]);
    }
}
