<?php

namespace App\Listeners\Users;

use App\Events\CommentsCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class CommentCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentsCreated  $event
     * @return void
     */
    public function handle(CommentsCreated $event)
    {
        $event->comment->created_by = Auth::user()->name;
        $event->comment->save();
    }
}
