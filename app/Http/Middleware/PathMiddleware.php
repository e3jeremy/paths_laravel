<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Path;

use App\Http\Resources\PathResource;

class PathMiddleware
{    /**
     * Traits
     */
    use \App\Http\Traits\Store;

    public function __construct (
        \Illuminate\Session\Store $store
        )
    {
        $this->store = $store;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lug = $request->segment(2);

        if(count($request->segments()) == 2) {
            $path = '';
            if($path = Path::where('slug', $lug)->with('options')->first()) $path = collect(new PathResource($path))->toArray();

            $this->storeOrReplace(['path' => $path]);
        }

        if ($lug != 'default' && !$this->fetchStore('path.status')) abort(404, 'Sorry Besh, The page You are looking not found');

        return $next($request);
    }
}
