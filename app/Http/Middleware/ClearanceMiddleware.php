<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class ClearanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if user has this permission
        if (Auth::user()->hasPermissionTo('Administer Paths')) {
            //If user is creating a path
            if ($request->is('api/submitPathForm')) {
                if (!Auth::user()->hasPermissionTo('Create Path')) {
                    return response ('UnAuthorized',401);
                } else {
                    return $next($request);
                }
            }
            //If user is editing a path
            if ($request->is('api/editPath/*')) {
                if (!Auth::user()->hasPermissionTo('Edit Path')) {
                    return response('UnAuthorized', 401);
                } else {
                    return $next($request);
                }
            }
            //If user is deleting a path
            if ($request->is('api/deletePath')) {
                if (!Auth::user()->hasPermissionTo('Delete Path')) {
                    return respomse('UnAuthorized', 401);
                } else {
                    return $next($request);
                }
            }
        }
        return $next($request);
    }
}
