<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class NlrTokenMiddleware
{
    protected $tokenHandler;

    public function __construct(
        \App\Http\Services\NLRToken $tokenHandler
        )
    {
        $this->tokenHandler = $tokenHandler;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('nlr.leadreactor_token')) {
            // This settings was copied from old paidforresearch.com
            $this->tokenHandler->setEncryptionApllied(config('nlrtoken.encryption_applied'))
                               ->setEncryptionKey(config('nlrtoken.encryption_key'))
                               ->setMacKey(config('nlrtoken.mac_key'))
                               ->setTokenEmail(config('nlrtoken.token_email'))
                               ->setTokenPassword(config('nlrtoken.token_password'))
                               ->setTokenUrl(config('nlrtoken.lead_reactor_url'));

            $request->session()->put('nlr.leadreactor_token', $this->tokenHandler->getToken());

            // If curl throws exceptions
            if($this->tokenHandler->hasError()) $request->merge(['token_error' => $this->tokenHandler->error()]);
        }

        return $next($request);
    }
}
