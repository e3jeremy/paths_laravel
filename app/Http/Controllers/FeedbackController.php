<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store;

class FeedbackController extends Controller
{
    /**
     * Traits
     */
    use \App\Http\Traits\Store;

    /**
     *  Required fields
     *
     * @var string
     */
    protected $params = [
        'first_name' => '',
        'last_name' => '',
        'email' => '',
        'address' => '',
        'city' => '',
        'state' => '',
        'zip' => '',
        'category_type' => '',
        'feedback' => '',
        'error' => '',
        'zip_status' => ''
    ];

    /**
     * Instantiate and determine if path is active.
     *
     * @param Illuminate\Session\Store      $store
     */
    public function __construct (
        Store $store
        )
    {
        if(app('request')->segment(2) != 'email-recipients' && app('request')->segment(2) != 'add-email-recipients') $this->middleware('isPathActive');

        // Store to property
        // Use by Traits\Store
        $this->store = $store;
    }

    /**
     * Feedback page
     *
     * @param  Illuminate\Http\Request      $request
     * @param  string                       $slug
	 * @return Illuminate\View\View
     */
    public function feedback (
        Request $request,
        $slug
        )
    {
        // Redirect to landing page when the path slug in url was change ..
        // if($this->fetchStore('session_slug') != $this->fetchStore('path.slug')) {
        if($request->segment(2) != $this->fetchStore('path.slug')) {
            // forget filter question in order for landing page to call an api fo it again.
            $this->store->forget('filter_questions');

            return  redirect()->route($slug .'-landing', $request->all());
        };

        $params = array_merge($this->params, $request->all());
        if($this->storeHas('user')) $params = array_merge($params, $this->fetchStore('user'));

        if($request->has('mail_error')) $params['error'] = $request->get('mail_error') . '.  Please Try again later.';

        if($request->has('input-comments')) $params['category_type'] = $request->get('input-comments');
        if($request->has('feedback')) $params['feedback'] = $request->get('feedback');

        return view('feedback')->with($params);
    }

    public function emailRecipients(
        Request $request,
        \App\EmailRecipient $emailRecipient
    ) {
        $rows = $emailRecipient->all();

        $emails = [];
        foreach($rows as $index => $email) {
            $emails[$index]['label'] = $email->full_name;
            $emails[$index]['value'] = $email->email;
            $emails[$index]['text'] = $email->email;
        }

        return $emails;
    }

    public function addEmailRecipients(
        Request $request,
        \App\EmailRecipient $emailRecipient
    ) {
        try {
            $emailRecipient->full_name = $request->get('full_name');
            $emailRecipient->email = $request->get('email');
            $emailRecipient->save();

            return ['error' => false, 'msg' => ''];
        } catch (\Exception $error) {
        return ['error' => true, 'msg' => $error->getMessage()];
        }




    }

    /**
     * Process user's feedback
     *
     * @param  Illuminate\Http\Request      $request
     * @param  string                       $slug
	 * @return Illuminate\View\View
     */
    public function processFeedback (
        Request $request,
        $slug
        )
    {
        if($request->get('zip_status') == 'invalid') return $this->feedback($request, $slug);

        $params = array_merge($this->params, $request->all());
        if($this->storeHas('user')) $params = array_merge($params, $this->fetchStore('user'));

        $params['last_visited'] = $this->fetchStore('_previous.url');
        if($request->has('current_page')) $params['last_visited'] = $this->fetchStore('nlr.path_url') . '/' . $request->get('current_page');

        $params['browser_info'] = 'Using ' . $this->fetchStore('nlr.browser.browser') . " " . $this->fetchStore('nlr.browser.browser_version') .' on ' . $this->fetchStore('nlr.browser.user_agent');
        $params['php_session'] = $this->storeID();
        $params['session_json'] = json_encode($this->stored());
        $params['category_type'] = $request->get('input-comments');
        $params['feedback'] = $request->get('feedback');
        $params['addcode'] = 'CD' . (($this->fetchStore('revenue_tracker_id')) ? $this->fetchStore('revenue_tracker_id') : 1);
        $params['ip'] = $request->ip();


        $pNum = 0;
        $pagePath = '';
        if($this->storeHas('campaigns')) {
          foreach($this->fetchStore('campaigns') as $content) {
            $pagePath .= 'Page ' . ++ $pNum . ': ' . implode(', ', $content) . '<br>';
          }
        }
        $params['page_path'] = $pagePath;


        $emails = 'jeremie@engageiq.com';
        if($this->Storehas('path.options.feedback.email_recipients')) {
            $emails = $this->fetchStore('path.options.feedback.email_recipients');
        }
        // Do the Email
        try {
            \Mail::send(
                'emails.feedback',
                $params,
                function ($mail){
                    $mail->from('noreply@engageiq.com')
                         ->to($emails)
                         ->subject('New feedback for a survey page on NLR');
                }
            );
        }
        catch (\Exception $exception) {
            $request->merge(['mail_error' => $exception->getMessage()]);
        }

        return $this->feedback($request, $slug);
    }
}
