<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentsResource;
use App\Events\CommentsCreated;
use App\Comment;
use App\Bug;
use Auth;

class CommentController extends Controller
{
    public function submitComment(CommentRequest $request, $id)
    {
        $comment = new Comment([
            'comments' => $request->input('comments')
        ]);
        $bug = Bug::find($id);
        $bug->comments()->save($comment);
        event(new CommentsCreated($comment));
        return response()->json(CommentsResource::collection(Comment::where('id', $comment->id)->get()));
    }
    public function getRelatedComments($id)
    {
        return response()->json(CommentsResource::collection(Comment::where('bug_id', $id)->get()));
    }
    /**
     * delete related comment by a user/author
     */
    public function deleteRelatedComment($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
    }
}
