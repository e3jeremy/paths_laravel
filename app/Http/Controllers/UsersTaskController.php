<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bug;

class UsersTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * return all users tasks
     */
    public function index()
    {
        return Bug::with('user')->paginate(5);
    }
}
