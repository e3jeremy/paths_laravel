<?php

namespace App\Http\Controllers;

use App\Option;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

use App\Http\Services\Paths\Preview;

class StyleController extends Controller
{
    protected $pathStyle;

    public function __construct(
        Filesystem $file,
        \App\Http\Services\PathStyle $pathStyle
        )
    {
        $this->file = $file;
        $this->pathStyle = $pathStyle;
    }


    public function get(
        Request $request,
        $slug
        )
    {

        $editor = resource_path() . '/assets/sass/paths/' . $slug . '/_editor.scss';
        $backup = resource_path() . '/assets/sass/paths/' . $slug . '/_backup.scss';
        $temp = resource_path() . '/assets/sass/paths/' . $slug . '/_temp.scss';

        return [
                'editor' => $this->file->get($editor),
                'editor_size' => $this->file->size($editor),
                'backup' => $this->file->get($backup),
                'backup_size' => $this->file->size($backup),
                'temp_size' => $this->file->size($temp)
            ];
    }
    
    public function update(
        Request $request,
        \App\Http\Services\Paths\Options $options,
        $pathID
        )
    {
        // Update editor stylesheet
        $this->file->put($this->pathStyle->getResourcePath($request->get('slug'). '/_editor.scss') , $request->get('content'));

        // Update stylesheet for compliling
        $this->pathStyle->updateStylesheet(
            $request->get('slug'),
            ($color = $request->get('color')) ? $color: '#c2cfd6',
            $request->get('template'),
            ($request->get('progress_bar_type') == 'custom') ? '@import \'../../progressbar/' . $request->get('custom_file_name') . '\';' : ''
        );

        // compile to public path
        $response = $this->pathStyle->compile($this->pathStyle->getResourcePath($request->get('slug'). '/'), $this->pathStyle->getPublicPath());

        // Update vesion to render new stylesheet, not from cache.
        if(!$response['error']) {
            $options->saveInputs(
                $pathID, ['section' => 'style', 'version' => 'v' . time()]
            );
        }

        return $response;
    }

    public function backupStylesheet(
        Request $request,
        $slug
        )
    {
        return $this->pathStyle->backup($slug);
    }

    public function revertStylesheetBackup(
        Request $request,
        $slug
        )
    {
        return $this->pathStyle->revert($slug);
    }

    public function applyReversionStylesheetBackup(
        Request $request,
        Option $option,
        $pathID
        )
    {
        $insert = [
            'path_id' => $pathID,
            'key' => 'version',
            'value' => 'v' . time() ,
            'section' => 'style'
        ];
        $option->updateOrCreate(['key' => 'version', 'path_id' => $pathID], $insert);

        $slug = $request->get('slug');

        return $this->pathStyle->applyReversion($slug);
    }

    public function cancelReversionStylesheetBackup(
        Request $request,
        $slug
        )
    {
        return $this->pathStyle->cancelReversion($slug);
    }
}
