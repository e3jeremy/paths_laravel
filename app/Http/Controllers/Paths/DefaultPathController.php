<?php

namespace App\Http\Controllers\Paths;

use App\Http\Controllers\Controller;

use App\Http\Services\Paths\DefaultLanding;
use App\Http\Services\Paths\DefaultRegistration;
use App\Http\Services\Paths\DefaultCampaign;
use App\Http\Services\Paths\ContentReplace;

use App\Http\Services\Helpers\Browser;
use App\Http\Services\Helpers\Guzzle;

use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Auth\AuthManager;

class DefaultPathController extends Controller
{
    /**
     * Traits
     */
    use \App\Http\Traits\Store,
        \App\Http\Traits\Phone,
        \App\Http\Traits\ProgressBar,
        \App\Http\Traits\PathAdditionalDetals;

    /**
     * Path url slug
     *
     * @var string
     */
    const PATH_SLUG = 'default';

    /**
     * Instantiate and determine if path is active.
     *
     * @param Illuminate\Session\Store      $store
     */
    public function __construct (
        Store $store,
        AuthManager $auth
        )
    {
        if(app('request')->segment(2) != 'session') $this->middleware('isPathActive');
        else $this->middleware('auth');

        $this->auth = $auth;

        // Store to property
        // Use by Traits\Store
        $this->store = $store;
    }

    /**
     * Landing page
     *
     * @param  Illuminate\Http\Request                  $request
     * @param  hisorange\BrowserDetect\Browser          $browser
     * @param  App\Http\Services\Helpers\Guzzle         $guzzle
     * @param  App\Http\Services\Paths\DefaultLanding    $paramHandler
	 * @return Illuminate\View\View
     */
    public function landing (
        Request $request,
        Guzzle $guzzle,
        Browser $browser,
        DefaultLanding $paramHandler
        )
    {
        // Flush old session
        // $this->store->flush();

        // Lead reactor url
        $this->storing('nlr.leadreactor_url', config('nlrtoken.lead_reactor_url'));

        // Path url
        $this->storing('nlr.path_url', $request->url());

        // Device
        $this->storing('nlr.device', $browser->device());
        $this->storing('nlr.device.type', $browser->type(array_search(1, $this->fetchStore('nlr.device'))));

        // Browser
        $this->storing('nlr.browser', $browser->browser());

        // Settings of general user info
        $paramHandler->setParams([
            $request->all(),
            $this->device(),
            $this->browser(),
            $request->fullUrl(),
            $request->ip()
        ]);

        // If Phone is available, we need to fix it....
        $this->fixPhone($paramHandler->params());

        // We need to update the params, there might be changes in phone...
        $paramHandler->addParams($this->phone());

        // Store the general user info co'z the next page will look up to this.
        // If the following pages can't find this profile,
        // it will all redirect to this page for profiling.
        $this->storeOrReplace(['profile' => $paramHandler->params()]);

        // Store the slug of current paths
        // If in the following pages, the slug was change, we will force them to go back to this page.
        // $this->storing('session_slug', $this->fetchStore('path.slug'));

        // Check if user will have profile questions
        if(!$this->hasFilterQuestions()) {
            // Get the filter questions
            $guzzle->setClientUrl($this->leadReactorUrl() . 'api/get_path_additional_details');
            $guzzle->setHeaders([ 'leadreactortoken' => $this->leadreactorToken()]);
            $guzzle->setQuery($paramHandler->params());
            $guzzle->request('GET');

            // Process the response
            // Bypass the errors thrown, we will leave $filters empty
            list($details, $filters, $icons) = $this->processPathAdditionalDetals($guzzle->responseContent());

            // Save to store
            $this->storing('nlr.path_details', 1);
            $this->storing('nlr.filter_icons', $icons);
            $this->storing('nlr.filter_questions', $filters);
            $this->storing('nlr.path_id', $details['path_id']);
            $this->storing('nlr.pixel.fire_at', $details['fire_at']);
            $this->storing('nlr.rev_tracker', $details['revenue_tracker_id']);
            $this->storing('nlr.pixel.postback', htmlentities($details['pixel']));
        }

        // Get path options.
        // Previously, the option was fetch from database and saved to store, see App\Http\Middleware\PathMiddleware
        $paramHandler->options->set($this->fetchStore('path.options'));

        // Progress
        $this->setProgress($this->storeHas('campaigns'), $this->fetchStore('campaigns'), 'landing');

        // Current Page
        $this->storing('current_page', 'landing');

        return $this->path('landing', [
            'is_stack_page' => false,
            'slug' => static::PATH_SLUG,
            'options' => $paramHandler->options->get(),
            'accepting' => $this->acceptingCount(),
            'progress' => round($this->progress()) . '%',
            'current_page' => $this->fetchStore('current_page'),
            'token_error' => ($request->has('token_error')) ? $request->get('token_error') : ''
        ]);
    }

    /**
     * Registration form page
     *
     * @param  Illuminate\Http\Request                       $request
     * @param  App\Http\Services\Paths\DefaultRegistration    $paramHandler
	 * @return Illuminate\View\View
     */
    public function registration(
        Request $request,
        DefaultRegistration $paramHandler
        )
    {
        // Redirect to landing page for fetching api token and profiling.
        // Request are also passed to page as query string
        if(!$this->hasToken() || !$this->storeHas('profile')) return redirect()->route(static::PATH_SLUG .'-landing', $request->all());

        // Redirect to landing page when the path slug in url was change ..
        // if($this->fetchStore('session_slug') != $this->fetchStore('path.slug')) {
        if($request->segment(2) != $this->fetchStore('path.slug')) {
            // forget filter question in order for landing page to call an api again.
            $this->store->forget('filter_questions');

            return  redirect()->route(static::PATH_SLUG .'-landing', $request->all());
        };

        // Setting of general user info
        $paramHandler->setParams(array_replace($this->fetchStore('profile'), $request->all()));

        // We need to update the profiler,
        // There might be changes on parameters
        $this->storeOrReplace(['profile' => $paramHandler->params()]);

        // Get path options.
        // Previously, the option was fetch from database and saved to store, see App\Http\Middleware\PathMiddleware
        $paramHandler->options->set($this->fetchStore('path.options'));

        // Progress
        $this->setProgress($this->storeHas('campaigns'), $this->fetchStore('campaigns'), 'registration');

        // Current Page
        $this->storing('current_page', 'registration');

        // Add parameters
        $paramHandler->addParams([
            'is_stack_page' => false,
            'slug' => static::PATH_SLUG,
            'agree_code' => $this->agreeCode(),
            'options' => $paramHandler->options->get(),
            'progress' => round($this->progress()) . '%',
            'current_page' => $this->fetchStore('current_page'),
            'action' => ($this->hasFilterQuestions()) ? '/paths/' .static::PATH_SLUG. '/question' : '',
            'campaign_error' => ($request->has('campaign_error')) ? $request->get('campaign_error') : ''
        ]);

        return $this->path('registration', $paramHandler->params());
    }

    /**
     * Process registration form submission
     *
     * @param  Illuminate\Http\Request                       $request
     * @param  App\Http\Services\Helpers\Guzzle              $guzzle
     * @param  App\Http\Services\Paths\DefaultRegistration    $paramHandler
     */
    public function processRegistration(
        Request $request,
        Guzzle $guzzle,
        DefaultRegistration $paramHandler
        )
    {
        // Setting of general user info
        // this time we dont need to update the profiler for it will not be use in next page.
        $paramHandler->setParams(array_replace($this->fetchStore('profile'), $request->all()));

        // Process submission from registration form
        if($request->get('submit') == 'eiq_registration')  {
            // Get campaigns
            $response = $this->processSubmission($guzzle, $paramHandler->params());

            // Redirect to campaign page
            if($this->hasCampaigns() && ! $guzzle->error()) return redirect()->route(static::PATH_SLUG . '-campaign', [1]);

            // If curl throws exceptions
            if($response['error'] && $guzzle->error()) $response['msg'] .= '<br /> Error: ' . strip_tags($guzzle->error());

            // Push to request, the registration form will catch this error through request
            $request->merge(['campaign_error' => ($response['msg'])]);
        }

        // Registration form
        return $this->registration($request, $paramHandler);
    }

    /**
     * Profile page
     *
     * @param  Illuminate\Http\Request                       $request
     * @param  App\Http\Services\Paths\DefaultRegistration    $paramHandler
	 * @return Illuminate\View\View
     */
    public function question(
        Request $request,
        DefaultRegistration $paramHandler
        )
    {
        // Redirect to landing page for fetching api token, fetch filter questions and profiling.
        // Request are also passed to page as query string
        if(!$this->hasToken() || !$this->hasFilterQuestions() || !$this->storeHas('profile')) return  redirect()->route(static::PATH_SLUG .'-landing', $request->all());

        // Make sure that the user is from registration page
        if($request->get('agree') != $this->fetchStore('agree_code')) return redirect()->route(static::PATH_SLUG .'-registration', $request->all());

        // Redirect to landing page when the path slug in url was change ..
        // if($this->fetchStore('session_slug') != $this->fetchStore('path.slug')) {
        if($request->segment(2) != $this->fetchStore('path.slug')) {
            // forget filter question in order for landing page to call an api fo it again.
            $this->store->forget('filter_questions');

            return  redirect()->route(static::PATH_SLUG .'-landing', $request->all());
        };

        // Setting of general user info
        $paramHandler->setParams(array_replace($this->fetchStore('profile'), $request->all()));

        // We need to update the profiler,
        // There might be changes on parameters
        $this->storeOrReplace(['profile' => $paramHandler->params()]);

        // Get path options.
        // Previously, the option was fetch from database and saved to store, see App\Http\Middleware\PathMiddleware
        $paramHandler->options->set($this->fetchStore('path.options'));

        // Progress
        $this->setProgress($this->storeHas('campaigns'), $this->fetchStore('campaigns'), 'question');

        // Current Page
        $this->storing('current_page', 'question');

        $paramHandler->addParams([
            'ip' =>$request->ip(),
            'is_stack_page' => false,
            'slug' => static::PATH_SLUG,
            'icons' => $this->filterIcons(),
            'source_url' =>$request->fullUrl(),
            'options' => $paramHandler->options->get(),
            'questions' => $this->filterQuestions(),
            'progress' => round($this->progress()) . '%',
            'current_page' => $this->fetchStore('current_page'),
            'campaign_error' => ($request->has('campaign_error')) ? $request->get('campaign_error') : ''
        ]);

		return $this->path('question', $paramHandler->params());
    }

    /**
     * Process submission from question page
     *
     * @param  Request             $request
     * @param  Guzzle              $guzzle
     * @param  DefaultRegistration $paramHandler
	 * @return Illuminate\View\View
     */
    public function processQuestion(
        Request $request,
        Guzzle $guzzle,
        DefaultRegistration $paramHandler
        )
    {
        // Setting of general user info
        // this time we dont need to update the profiler for it will not be use in next page.
        $paramHandler->setParams(array_replace($this->fetchStore('profile'),$request->all()));

        // Process submission from question page
        if($request->get('submit') == 'eiq_question') {

            // Get campaigns
            $response = $this->processSubmission($guzzle, $paramHandler->params());

            // Redirect to campaign page
            if($this->hasCampaigns() && ! $guzzle->error()) return redirect()->route(static::PATH_SLUG . '-campaign', [1]);

            // If curl throws exceptions
            if($response['error'] && $guzzle->error()) $response['msg'] .= '<br /> Error: ' . strip_tags($guzzle->error());

            // Push to request, the question form will catch this error through request
            $request->merge(['campaign_error' => ($response['msg'])]);
        }

        //Question form
        return $this->question($request, $paramHandler);
    }

    /**
     * Campaign page
     *
     * @param  Illuminate\Http\Request                   $request
     * @param  App\Http\Services\Helpers\Guzzle          $guzzle
     * @param  App\Http\Services\Paths\DefaultCampaign    $paramHandler
     * @param  integer                                   $page
	 * @return Illuminate\View\View
     */
    public function campaign(
        Request $request,
        Guzzle $guzzle,
        DefaultCampaign  $paramHandler,
        ContentReplace $contentReplace,
        $page
        )
    {
        // Note: we don't process $request here.

        // Redirect to landing page for fetching api token and profiling.
        // User index that was saved in store is from api response of get campaign lists.
        // No User index saved means no response from get campaign lists.
        // Request are also passed to redirected page page as query string
        if(!$this->hasToken() || !$this->storeHas('user')) return  redirect()->route(static::PATH_SLUG .'-landing', $request->all());

        // 404 page that exceed campaigns count
        if(!array_key_exists(($page - 1), $this->fetchStore('campaigns'))) abort(404);

        // Redirect to landing page when the path slug in url was change ..
        // if($this->fetchStore('session_slug') != $this->fetchStore('path.slug')) {
        if($request->segment(2) != $this->fetchStore('path.slug')) {
            // Forget filter question in order for landing page to call an api fo it again.
            $this->store->forget('filter_questions');
            // Forget saved user in order for user to register again.
            $this->store->forget('user');

            return  redirect()->route(static::PATH_SLUG .'-landing', $request->all());
        };

        // If Phone is available, we need to fix it....
        $this->fixPhone($this->fetchStore('user'));

        // We need to update the user, there might be changes in phone...
        $this->storeOrReplace(['user' => array_replace($this->fetchStore('user'), $this->phone())]);

        // Settings of campaign
        $paramHandler->setParams([
            'current_stack' => $page,
            'path_url' => $this->pathUrl(),
            'stack_count' => count($this->fetchStore('campaigns'))
        ]);

        // Create campaign links
        $paramHandler->setCampaigns($this->fetchStore('campaigns')[($page - 1)]);
        $paramHandler->setCreatives($this->fetchStore('creatives'));
        $paramHandler->generateCampaignLinks();

        // Fetch capaign content
        $guzzle->setClientUrl($this->leadReactorUrl() . 'api/get_stack_campaign_content_php');
        $guzzle->setHeaders([ 'leadreactortoken' => $this->leadreactorToken()]);
        $guzzle->setQuery(
            http_build_query(
                [
                    'path' => $this->pathID(),
                    'session' => $this->storeID(),
                    'affiliate_id' => $this->affiliateID()
                ]
            ) . '&' . $paramHandler->campaignLinks()
        );
        // Process restful service
        if($guzzle->request('GET')) {

            // Process replacing of code to actual data
            $contentReplace->process(
                // [], // uncomment this and comment the one below to see the actual code that was not replace.
                $this->replaceable(),
                $page,
                $guzzle->responseContent()
            );

            // Add parameters for eval in blade
            $paramHandler->addParams($this->variablesNeededB4Eval());
        }

        // Get path options.
        // Previously, the option was fetch from database and saved to store, see App\Http\Middleware\PathMiddleware
        $paramHandler->options->set($this->fetchStore('path.options'));

        // Progress
        $this->setProgress($this->storeHas('campaigns'), $this->fetchStore('campaigns'), 'campaign', $page);

        // Current Page
        $currentPage = $page;
        if($this->fetchStore('campaign_types')[($page - 1)] == 4) $currentPage = $this->fetchStore('campaigns')[($page - 1)][0];
        $this->storing('current_page', $currentPage);

        // Add parameters
        $paramHandler->addParams([
            'is_stack_page' => true,
            'slug' => static::PATH_SLUG,
            'content' => $contentReplace->html(),
            'options' => $paramHandler->options->get(),
            'phone' => $this->fetchStore('user.phone'),
            'progress' => round($this->progress()) . '%',
            'address' => $this->fetchStore('user.address'),
            'current_page' => $this->fetchStore('current_page')
        ]);

		return $this->path('campaign', $paramHandler->params());
    }

    /**
     * Process Session
     *
     * @param  Illuminate\Http\Request      $request
     */
    public function session (
        Request $request,
        $action
        )
    {
        if($action == 'print') {
            echo '<pre>';
            print_r($this->stored());
            echo '</pre>';
            return $this->stored();
        }

        if($action == 'store') $this->storeOrReplace(['user' => array_replace((Array)$this->fetchStore('user'), $request->all())]);

        return $this->fetchStore('user');
    }

    /**
     * Generate agree code.
     * We need random code so that no one can access question page without first filling up the registration form
     *
     * @return string
     */
    protected function agreeCode () {
        $char = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code = array(); //remember to declare $code as an array
        $alphaLength = strlen($char) - 1; //put the length -1 in cache
        for ($i = 0; $i < 25; $i++) {
            $rand = rand(0, $alphaLength);
            $code[] = $char[$rand];
        }
        $agreeCode = implode($code);
        $this->storing('agree_code', $agreeCode);

        return $agreeCode; //turn the array into a string
    }

    /**
     * Process the submission from either registration or question form
     *
     * @param  App\Http\Services\Helpers\Guzzle              $guzzle
     * @param  array                                         $inputs
     * @return array
     */
    protected function processSubmission ($guzzle, $inputs)
    {
        // Set the client url
        $guzzle->setClientUrl($this->leadReactorUrl() . 'api/get_campaign_list');
        // Set request headers
        $guzzle->setHeaders([ 'leadreactortoken' => $this->leadreactorToken()]);
        // Set request query string
        $guzzle->setQuery(
            implode(
                '&', array_map(
                    function ($value, $key) {
                        if(is_array($value)) return http_build_query([$key => $value]);

                        return sprintf('%s=%s', $key, $value);
                     },
                    $inputs,
                    array_keys(
                        $inputs
                    )
                )
            )
        );
        // Process restful service
        if($guzzle->request('GET')) {
            $this->storeOrReplace(collect($guzzle->responseContent())->toArray());

            return ['error' => false, 'msg' => ''];
        }

        return ['error' => true, 'msg' => 'No campaigns exists based on your provided input data.'];
    }

    /**
     * Determine the view template
     *
     * @param  string $page
     * @param  array $params
	 * @return Illuminate\View\View
     */
    protected function path($page, $params)
    {
        return view('default.path.'.$page)->with($params);
    }
}
