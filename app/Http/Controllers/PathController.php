<?php

namespace App\Http\Controllers;

use Auth;
use Storage;

use App\Path;
use App\Option;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

use App\Http\Resources\PathResource;

use App\Http\Requests\PathRequest;

use App\Events\PathsCreated;
use App\Events\PathsUpdated;

class PathController extends Controller
{

	public function __construct()
	{
		// $this->middleware(['auth', 'clearance']);
	}

	/**
	* Get all paths
	*
	* @param  Illuminate\Http\Request $request
	* @param  App\Path    $path
	* @return array
	*/
	public function allPaths(
		Request $request,
		Path $path
	) {
		return PathResource::collection($path->paginate(10));
	}

	/**
	* Create new path
	*
	* @param  PathRequest                          $request
	* @param  App\Http\Services\Paths\Options      $options
	* @param  App\Http\Services\PathController     $pathController
	* @param  App\Http\Services\PathBlade          $pathBlade
	* @param  App\Http\Services\PathBlade          $pathBlade
	* @param  App\Http\Services\PathRoute          $pathRoute
	* @param  App\Http\Services\PathStyle          $pathStyle
	* @return void
	*/
	public function createPath(
		PathRequest $request,
		\App\Http\Services\Paths\Options $options,
		\App\Http\Services\PathController $pathController,
		\App\Http\Services\PathBlade $pathBlade,
		\App\Http\Services\PathRoute $pathRoute,
		\App\Http\Services\PathStyle $pathStyle
	) {
		$slug = strtolower(str_replace(' ', '-', $request->input('name')));

		$path = Path::create([
			'name' => $request->input('name'),
			'description' => $request->input('description'),
			'slug' => $slug,
			'url' => url('paths/' . $slug),
		]);
		event(new PathsCreated($path));

		$folderName = $this->getFolderName();
		$controllerName = str_replace(' ', '', ucwords($request->input('name'))) . 'Controller';

		// Create contoller
		$pathController->create($slug, $controllerName, $folderName);
		// Create blade
		$pathBlade->create($slug, $folderName);
		// Create route
		$pathRoute->create($slug, $controllerName, $folderName);
		// Create stylesheet
		$pathStyle->create($slug);

		// Add ability to use production file in local environment
		if($folderName == 'devpaths') {
			$options->saveInputs ($path->id, [
				'section' => 'general',
				'use_production_file' => 'false',
			]);
			return;
		}


		// Execute version control
		// $cmd = 'chmod +x ' . base_path() . '/sh/add.path.sh';
		$cmd = 'sh ' . base_path() . '/sh/add.path.sh';
		$cmd .= " '" . $slug ."'";
		$cmd .= " '" . base_path() . "'";
		// $cmd .= " '" . ucwords($folderName) . "'";
		$cmd .= " 'Paths'";
		$cmd .= " '" . $controllerName . "'";
		$cmd .= " '" . resource_path() . "'";
		// $cmd .= " '" . $folderName . "'";
		$cmd .= " 'paths'";
		$cmd .= " 'Adding'";
		$cmd .= " '" . ucwords($request->input('name')) . "'";
		$cmd .= " '" . config('nlrtoken.git_username') . "'";

		exec($cmd);
	}

	/**
	* Delete path
	*
	* @param  AppHttpServicesPathController $pathController
	* @param  AppHttpServicesPathBlade      $pathBlade
	* @param  AppHttpServicesPathRoute      $pathRoute
	* @param  AppHttpServicesPathStorage    $pathStorage
	* @param  AppHttpServicesPathStyle      $pathStyle
	* @param  Path                          $path
	* @param  integer                       $pathID
	* @return array
	*/
	public function deletePath(
		\App\Http\Services\PathController $pathController,
		\App\Http\Services\PathBlade $pathBlade,
		\App\Http\Services\PathRoute $pathRoute,
		\App\Http\Services\PathStorage $pathStorage,
		\App\Http\Services\PathStyle $pathStyle,
		Path $path,
		$pathID
	) {
		if($path = $path->findOrFail($pathID)) {

			$folderName = $this->getFolderName();
			$controllerName = str_replace(' ', '', ucwords($path->name)) . 'Controller';
			$slug = $path->slug;

			$pathController->remove($controllerName, $folderName);
			$pathBlade->remove($slug, $folderName);
			$pathRoute->remove($slug, $folderName);
			$pathStorage->remove($slug);
			$pathStyle->remove($slug);

			$path->delete();

			if($folderName != 'devpaths') {
				// Execute version control
				$cmd = 'sh ' . base_path() . '/sh/add.path.sh';
				$cmd .= " '" . $slug ."'";
				$cmd .= " '" . base_path() . "'";
				// $cmd .= " '" . ucwords($folderName) . "'";
				$cmd .= " 'Paths'";
				$cmd .= " '" . $controllerName . "'";
				$cmd .= " '" . resource_path() . "'";
				// $cmd .= " '" . $folderName . "'";
				$cmd .= " 'paths'";
				$cmd .= " 'Removing'";
				$cmd .= " '" .  ucwords($path->name) . "'";
				$cmd .= " '" . config('nlrtoken.git_username') . "'";

				exec($cmd);
			}

			return [
				'error' => false
			];
		}
		return [
			'error' => true,
			'msg' => 'Unable to remove the path co\'z it\'s unavailable.'
		];
	}

	/**
	* Get path details
	*
	* @param  Request $request
	* @param  Path    $path
	* @param  integer  $pathID
	* @return Illuminate\Http\JsonResponse
	*/
	public function getDetails(
		Request $request,
		Path $path,
		$pathID
	) {
		if($row = $path->with(['options'=> function($q) {
			$q->select('path_id', 'key', 'value', 'section');
		}])->find($pathID)) {

			return response()->json(new PathResource($row));
		}
		return response()->json([]);
	}

	/**
	* Update pth getDetails
	*
	* @param  Request $request
	* @param  Path    $path
	* @param  integer  $pathID
	* @return array
	*/
	public function update(
		Request $request,
		Path $path,
		$pathID
	) {
		if($row = $path->find($pathID)) {
			foreach($request->all() as $field => $value) {
				if($field != 'id'){
					$row->$field = $value;
				}
			}
			$row->save();
			return [
				'error' => false
			];
		}

		return [
			'error' => true,
			'msg' => 'Can\'t update for the path is unavailable.'
		];

	}

	/**
	* Update path status
	*
	* @param  Request $request
	* @param  integer  $id
	* @return Illuminate\Http\Response
	*/
	public function updateStatus(
		Request $request,
		$id
	) {
		$path = Path::find($id);

		if ($request->has('status')) {
			$path->status = $request->input('status')  == 1 ? 0: 1;
		}

		$path->save();
		event(new PathsUpdated($path));

		return response($path);
	}

	/**
	* Preview image in storage
	*
	* @param  Request                $request
	* @param  AppHttpServicesStorage $storage
	* @param  string                 $slug
	* @param  string                 $image
	* @return void
	*/
	public function previewImage(
		Request $request,
		\App\Http\Services\Storage $storage,
		$slug,
		$image
	) {
		if(!$storage->exists($slug . '/' . $image)) return abort('404');

		header("Content-type: image/png");
		echo $storage->getImage($slug . '/' . $image);
	}

	/**
	* Get folder name
	*
	* @return string
	*/
	protected function getFolderName ()
	{
		if(app('env') == 'production') return 'paths';
		return 'devpaths';
	}
}
