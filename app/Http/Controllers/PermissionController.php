<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Http\Requests\PermissionRequest;
use App\Http\Resources\PermissionResource;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin', 'isSystemAdministrator'])->except(['permissions']);
    }
    /**
     * show permissions
     */
    public function permissions()
    {
        return PermissionResource::collection(Permission::all());
    }
    public function createPermissions(PermissionRequest $request)
    {
        $role = Permission::create([
            'name' => $request->input('name')
        ]);
    }
    /**
     *  update Permission Request $request
     */
    public function updatePermission(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);

        $input = $request->all();
        $permission->fill($input)->save();
    }
    /**
     * delete permission
     */
    public function deletePermission($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
    }
}
