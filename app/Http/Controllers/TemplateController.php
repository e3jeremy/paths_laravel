<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

use App\Http\Services\Paths\Preview;

class TemplateController extends Controller
{
    protected $file;

    /**
     * [__construct description]
     */
    public function __construct (
        Filesystem $file
        )
    {
        $this->file = $file;
    }

    /**
     * [landing description]
     * @param  Request        $request
     * @param  NLRToken       $tokenHandler
     * @param  DefaultLanding $default
     * @return [type]
     */
    public function preview(
        Request $request,
        Preview $preview,
        $template
        )
    {
        $preview->setParams([
            'template' => $template
        ]);

		return view('templates.preview')->with($preview->params());
    }

    public function templates(
        Request $request
        )
    {
        $templates = [];

        if($this->file->exists($this->getPath('templates'))) {
            foreach ($this->file->directories($this->getPath('templates')) as $directory)
            {
                $slug = $this->file->name($directory);

                $templates[$slug]= parse_ini_file($directory . '/info.ini');
                $templates[$slug]['option_inputs']= $this->parseIniFile($directory . '/options.ini', true);
                $templates[$slug]['slug'] = $slug;
                $templates[$slug]['path'] = $directory;
                $templates[$slug]['screenshot_src'] = $this->screenshotSrc($directory, $slug);
                $templates[$slug]['screenshot_src'] = $this->screenshotSrc($directory, $slug);
            }
        }

        return $templates;
    }

    public function customProgressBar(
        Request $request
        )
    {
        $progressBars = [];

        if($this->file->exists($this->getPath('progressbar'))) {
            foreach ($this->file->allFiles($this->getPath('progressbar')) as $file)
            {
                $blade = $this->file->name($file);
                $name = str_replace('.blade', '', strtolower($blade));

                $progressBars[$name]['name'] = $name;
                $progressBars[$name]['filename'] = $blade . '.php';
                $progressBars[$name]['path'] = $file->getPathName();
            }
        }

        return $progressBars;
    }

    public function trackers(
        Request $request
        )
    {
        // $this->templateController->customProgressBar($request);
        $ptrackers = [];
        // exit;
        if($this->file->exists($this->getPath('trackers'))) {
            foreach ($this->file->allFiles($this->getPath('trackers')) as $file)
            {
                $blade = $this->file->name($file);
                // $slug = $this->file->name($directory);
                $name = str_replace('.blade', '', strtolower($blade));

                $ptrackers[$name]['name'] = $name;
                $ptrackers[$name]['filename'] = $blade . '.php';
                $ptrackers[$name]['path'] = $file->getPathName();
            }
        }

        return $ptrackers;
    }

    protected function screenshotSrc ($directory, $slug)
    {
        $path = $directory . '/screenshot.png';

        $imgData = $this->file->get($path);
        $type = $this->file->extension($path);
        return 'data:image/' . $type . ';base64,' . base64_encode($imgData);
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getPath($fileName = null)
    {
        /** @var \Illuminate\View\FileViewFinder $viewFinder */
        $viewFinder = app('view.finder');

        $paths = $viewFinder->getPaths();

        // If we have more than one path configured, throw an
        // exception as this is currently not supported by
        // the package. It might be supported later on.
        if (count($paths) !== 1) {
            //throw UnsupportedException::tooManyPaths(count($paths));
        }

        $path = reset($paths);

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * Parse Options
     *
     * @param  file  $file
     * @param  boolean $processSections
     * @param  string  $scannerMode
     * @return array
     */
    protected function parseIniFile($file, $processSections = 0, $scannerMode = INI_SCANNER_NORMAL) {
        $explodeStr = '.';
        $escapeChar = "'";
        // load ini file the normal way
        $data = parse_ini_file($file, $processSections, $scannerMode);
        if (!$processSections) {
            $data = array($data);
        }
        foreach ($data as $sectionKey => $section) {
            // // loop inside the section
            // echo '<pre>';
            // print_r($section);
            // echo '</pre>';
            // exit;
            foreach ($section as $key => $value) {
                if (strpos($key, $explodeStr)) {
                    if (substr($key, 0, 1) !== $escapeChar) {
                        // key has a dot. Explode on it, then parse each subkeys
                        // and set value at the right place thanks to references
                        $subKeys = explode($explodeStr, $key);
                        $subs =& $data[$sectionKey];
                        foreach ($subKeys as $subKey) {
                            if (!isset($subs[$subKey])) {
                                $subs[$subKey] = [];
                            }
                            $subs =& $subs[$subKey];
                        }
                        // set the value at the right place
                        $subs = $value;
                        // unset the dotted key, we don't need it anymore
                        unset($data[$sectionKey][$key]);
                    }
                    // we have escaped the key, so we keep dots as they are
                    else {
                        $newKey = trim($key, $escapeChar);
                        $data[$sectionKey][$newKey] = $value;
                        unset($data[$sectionKey][$key]);
                    }
                }
            }
        }
        if (!$processSections) {
            $data = $data[0];
        }
        return $data;
    }

}
