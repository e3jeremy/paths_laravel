<?php

namespace App\Http\Controllers;

use Storage;

use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemManager;

class OptionController extends Controller
{
    /**
     * Traits
     */
    use \App\Http\Traits\Store;


    public function __construct(
        Store $store,
        Filesystem $file,
        FilesystemManager $storage,
        \App\Http\Services\PathStyle $pathStyle,
        \App\Http\Services\Paths\Options $options
        )
    {
        $this->store = $store;
        $this->file = $file;
        $this->storage = $storage;
        $this->pathStyle = $pathStyle;
        $this->options = $options;
    }


    public function optionRemove (
        Request $request,
        $pathID
        )
    {
        if ($request->has('key')) {
            // Delete selected option
            if($this->options->remove($pathID, $request->get('key'))) return ['error' => false];
        }

        return [
            'error' => true,
            'msg' => 'Unable to remove the option.'
        ];
    }

    public function optionGeneral (
        Request $request,
        \App\Http\Services\Storage $storage,
        $pathID
        )
    {
		// if($request->has('use_production_file')) $this->updateEnvironment($request->get('slug'), $request->get('use_production_file'));

        $imgData = $storage->StoreImage($request, 'main_bg.png', 'body_background_img');

        $this->options->saveInputs($pathID, array_merge($request->all(), $imgData));

        return [
            'error' => false
        ];
    }

    public function optionTheme (
        Request $request,
        $pathID
        )
    {
        $required = ['color', 'secondary_color', 'info_color', 'success_color', 'warning_color', 'danger_color', 'template'];

        $section = ($request->get('section')) ? $request->get('section') : 'themes';
        $template = ($request->get('template')) ? $request->get('template') : 'default';

        $path = $this->getPath('templates/default/options.ini');

        if($this->file->exists($path)) {
                $options= $this->parseIniFile($path, true, $section);
                $required = array_keys($options[$section]);
        }

        $this->options->saveInputs($pathID, $request->all(), $required);

        // Update stylesheet for compliling
        $this->pathStyle->updateStylesheet(
            $request->get('slug'),
            ($color = $request->get('color')) ? $color: '#3097D1',
            $request->get('template'),
            ($request->get('progress_bar_type') == 'custom') ? '@import \'../../progressbar/' . $request->get('custom_file_name') . '\';' : ''
        );

        // compile to public path
        $response = $this->pathStyle->compile($this->pathStyle->getResourcePath($request->get('slug') . '/'), $this->pathStyle->getPublicPath());

        // Update vesion to render new stylesheet, not from cache.
        if(!$response['error']) {
            $this->options->saveInputs(
                $pathID, ['section' => 'style', 'version' => 'v' . time()]
            );
        }

        return $response;
    }

    /**
     * Save options for header
     *
     * @param  Illuminate\Http\Request   $request
     * @param integer                    $pathID
     */
    public function optionHeader (
        Request $request,
        \App\Http\Services\Storage $storage,
        $pathID
        )
    {

        $imgData = $storage->StoreImage($request, 'header_bg.png', 'header_background_img');

        $this->options->saveInputs($pathID, array_merge($request->all(), $imgData));

        return [
            'error' => false
        ];
    }

    public function optionFooter (
        Request $request,
        $pathID
        )
    {
        $this->options->saveInputs($pathID, $request->all());

        return [
            'error' => false
        ];
    }

    public function optionProgressBar (
        Request $request,
        $pathID
        )
    {
        $required = ['show_progress_bar', 'progress_bar_type', 'progress_bar_height', 'custom_file_name'];

        $section = ($request->get('section')) ? $request->get('section') : 'progress_bar';
        $template = ($request->get('template')) ? $request->get('template') : 'default';

        $path = $this->getPath('templates/default/options.ini');

        if($this->file->exists($path)) {
                $options= $this->parseIniFile($path, true, $section);
                $required = array_keys($options[$section]);
        }

        $this->options->saveInputs($pathID, $request->all(), $required);

        // Update stylesheet for compliling
        $this->pathStyle->updateStylesheet(
            $request->get('slug'),
            ($color = $request->get('color')) ? $color: '#3097D1',
            $request->get('template'),
            ($request->get('progress_bar_type') == 'custom') ? '@import \'../../progressbar/' . $request->get('custom_file_name') . '\';' : ''
        );

        // compile to public path
        $response = $this->pathStyle->compile($this->pathStyle->getResourcePath($request->get('slug'). '/'), $this->pathStyle->getPublicPath());

        // Update vesion to render new stylesheet, not from cache.
        if(!$response['error']) {
            $this->options->saveInputs(
                $pathID, ['section' => 'style', 'version' => 'v' . time()]
            );
        }

        return $response;
    }

    public function optionNavigationBar (
        Request $request,
        \App\Http\Services\Storage $storage,
        $pathID
        )
    {
        $imgData = $storage->StoreImage($request, 'logo.png', 'nav_bar_logo');

        $this->options->saveInputs($pathID, array_merge($request->all(), $imgData));

        return [
            'error' => false
        ];
    }

    public function optionContent (
        Request $request,
        \App\Http\Services\Storage $storage,
        $pathID
        )
    {
        $imgData = $storage->StoreImage($request, 'content_bg.png', 'content_background_img');

        $this->options->saveInputs($pathID, array_merge($request->all(), $imgData));

        return [
            'error' => false
        ];
    }

    public function optionTrackers (
        Request $request,
        $pathID
        )
    {
        $this->options->saveInputs($pathID, $request->all());

        return [
            'error' => false
        ];
    }

    public function OptionFeedback (
        Request $request,
        $pathID
        )
    {
        $this->options->saveInputs($pathID, $request->all());

        return [
            'error' => false
        ];
    }/**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getPath($fileName = null)
    {
        /** @var \Illuminate\View\FileViewFinder $viewFinder */
        $viewFinder = app('view.finder');

        $paths = $viewFinder->getPaths();

        // If we have more than one path configured, throw an
        // exception as this is currently not supported by
        // the package. It might be supported later on.
        if (count($paths) !== 1) {
            //throw UnsupportedException::tooManyPaths(count($paths));
        }

        $path = reset($paths);

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    protected function parseIniFile($file, $processSections = 0, $selectedSection ='', $scannerMode = INI_SCANNER_NORMAL) {
        $explodeStr = '.';
        $escapeChar = "'";
        // load ini file the normal way
        $data = parse_ini_file($file, $processSections, $scannerMode);

        if($selectedSection) $data = [$selectedSection => $data[$selectedSection]];

        foreach ($data as $sectionKey => $section) {
            // // // loop inside the section
            foreach ($section as $key => $value) {
                if (strpos($key, $explodeStr)) {
                    if (substr($key, 0, 1) !== $escapeChar) {
                        // key has a dot. Explode on it, then parse each subkeys
                        // and set value at the right place thanks to references
                        $subKeys = explode($explodeStr, $key);
                        if(end($subKeys) == 'name') {
                            $indx = $value;
                            $data[$sectionKey][$value] = '';
                        }
                        if(end($subKeys) == 'default') $data[$sectionKey][$indx] = $value;

                        // unset the dotted key, we don't need it anymore
                        unset($data[$sectionKey][$key]);
                    }
                }
            }
        }
        return $data;
    }

}
