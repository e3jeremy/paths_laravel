<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BugRequest;
use App\Bug;
use App\Http\Resources\BugResource;
use App\Events\BugsCreated;
use App\Events\BugsUpdated;
use App\User;

class BugController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isQA', 'isSystemAdministrator'])->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Bug $bug)
    {
        $open = $bug->where('status', 1)->count();
        $close = $bug->where('status', 0)->count();
        $searchItem = $request->input('searchItem');
        /**
         * the closure below will allow us to to apply where clause only if input request is not null, otherwise, it will just skip the closure
         */
        return BugResource::collection(Bug::with('user')->when($searchItem, function($query) use ($searchItem) {
            return $query->where('name', 'like', $searchItem.'%')->orWhere('description', 'like', $searchItem.'%');
        })->paginate(10))
            ->additional(['meta' => [
                    'open' => $open,
                    'close' => $close,
                ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\BugRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BugRequest $request)
    {
        $bug = new Bug;
        $bug->name = $request->input('name');
        $bug->description = $request->input('description');
        $bug->type = $request->input('type');
        $bug->bugnumber = str_random(7);
        $user = User::find($request->input('assignto'));
        $user->bugs()->save($bug);
        event(new BugsCreated($bug));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bug = Bug::find($id);
        $assignto = $request->input('assignto');
        if ($request->has('status')) {
            $bug->status = $request->input('status')  == 1 ? 0: 1;
        } else if($request->has('assignto')) {
            $assignto = $request->input('assignto');
            $userId = \App\User::where('name', trim($assignto))->first();
            $bug->assignto = $userId->id;
        }

        $bug->save();
        event(new BugsUpdated($bug));
        return response($bug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
