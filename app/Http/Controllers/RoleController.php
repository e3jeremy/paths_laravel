<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use App\Http\Resources\RoleResource;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isSystemAdministrator'])->except(['roles']);
    }
    /**
     * show roles
     * @var resource
     */
    public function roles()
    {
        return RoleResource::collection(Role::all());
    }
    /**
     * create new roles and assign permissions
     * @request RoleRequest
    */
    public function createRoles(RoleRequest $request)
    {
        $permissions = $request->input('permissions');
        $name = $request->input('name');

        $role = Role::create([
            'name' => $name
        ]);

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }
    }
    /**
     * update Roles by id
     */
    public function updateRoles(RoleRequest $request, $id)
    {
        //Get role with the given id
        $role = Role::findOrFail($id);

        $input = $request->except(['permissions']);
        $permissions = $request->input('permissions');
        $role->fill($input)->save();

        //Get all permissions
        $p_all = Permission::all();

        //Remove all permissions associated with role
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }

        /**
         * Get corresponding form //permission in db
         * Assign permission to role
         */
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role->givePermissionTo($p);
        }
    }
    /**
     * delete Role Request $request $role
     */
    public function deleteRole($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return response()->json(['success' => 'success']);
    }

}
