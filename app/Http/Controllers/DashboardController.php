<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'clearance']);
    }
    /**
     * Dashboard area
     */
    public function index()
    {
		return view('dashboard');
    }

    /**
     * show specific path
     */
    public function show()
    {

    }
}
