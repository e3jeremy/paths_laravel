<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class ProjectDocumentationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isDeveloper', 'isSystemAdministrator'])->except(['docs']);
    }
    /**
     * Update docs
     */
    public function updateDocs(Request $request)
    {
        if ($request->hasFile('md')) {
            Storage::disk('local')->put('docs.md', file_get_contents($request->file('md')));
            $tempMd = Storage::disk('local')->get('docs.md');
            return response()->json(['docs' => $tempMd ]);
        }
        Storage::disk('local')->put('docs.md', $request->input('docs'));
        return response()->json(['docs' => $request->input('docs')]);
    }
    /**
     * get saved docs from file
     */
    public function docs()
    {
        $docs = Storage::disk('local')->get('docs.md');
        return response()->json(['docs' => $docs]);
    }
}
