<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

class ColorController extends Controller
{
    protected $file;

    protected $colors = [
        'blue' => '#20a8d8',
        'default' => '#c2cfd6',
        'green' => '#4dbd74',
        'pink' => '#e83e8c',
        'purple' => '#6f42c1',
        'red' => '#f86c6b',
        'yellow' => '#ffc107',
        'cyan' => '#63c2de'
    ];

    /**
     * [__construct description]
     */
    public function __construct (
        Filesystem $file
        )
    {
        $this->file = $file;
    }

    public function colors(
        Request $request
        )
    {
        $colors = [];
        $indx = 0;

        foreach ($this->colors as $name => $code)
        {
            $colors[$indx]['name'] =  $name;
            $colors[$indx]['path'] = $code;
            $indx ++;
        }

        return $colors;
    }
}
