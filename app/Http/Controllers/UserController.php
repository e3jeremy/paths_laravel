<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;
use App\Mail\PleaseConfirmYourEmail;
use App\Events\UsersCreated as Created;
use App\Events\UsersUpdated;

class UserController extends Controller
{
    /**
     *  guest users can't see this route
     */
    public function __construct()
    {
        $this->middleware(['auth', 'isSystemAdministrator'])->except(['index']);
    }
    /**
     * show users
     * @var resource
     */
    public function index()
    {
        $items = UserResource::collection(User::paginate(10));
        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];
        return response()->json($response);
    }
    /**
     * create new user
     */
    public function createUser(UserRequest $request)
    {
        $randomPassword = str_random(strlen($request->input('password')));
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($randomPassword),
            'confirmation_token' => str_limit(md5($request->input('email').str_random()), 25, ''),
            'api_token' => str_random(60),
        ]);
        $roleId = $request->input('role');
        $roleR = Role::where('id', $roleId)->firstOrFail();
        $user->assignRole($roleR);
        $emailUser = User::findOrFail($user->id);
        //Mail::to($emailUser->email)->send(new UserCreated($emailUser, $randomPassword));
        Mail::to($user)->send(new PleaseConfirmYourEmail($user, $randomPassword));
        event(new Created($user));
        return response()->json($user);
    }
    /**
     *  edit user
     * @param $request Request
     * @param $id int
     */
    public function edit(UpdateRoleRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $role = $request->input('role');
        if (!empty($role)) {
            $user->roles()->sync($role);
        }
        event(new UsersUpdated($user));
    }
    /**
     * deactivate a User
     * Once deactivated, User no longer access in any guarded routes
     * @var $request Request
     */
    public function deactivateUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $status = $request->input('status');
        if ($status == '0') {
            $user->status = '1';
        } else {
            $user->status = '0';
        }
        $user->save();
        event(new UsersUpdated($user));
    }
}
