<?php

namespace App\Http\Traits;

Trait Phone {

    protected $phone = [
        'phone' => '',
        'phone1' => '' ,
        'phone2' => ''  ,
        'phone3' => ''
    ];

    /**
     * [phone description]
     * @return [type] [description]
     */
    public function phone()
    {
        return $this->phone;
    }

    /**
     * [fixPhoneInParams description]
     * @return [type]
     */
    protected function fixPhone ($param)
    {
        if($param['phone'] != '') {
            if(strlen($phoneNum = preg_replace("/[^0-9,.]/", "", $param['phone'])) == 10) {
                $this->phone['phone'] = $param['phone'];
                $this->phone['phone1'] = substr($phoneNum, 0, 3);
                $this->phone['phone2'] = substr($phoneNum, 3, 3);
                $this->phone['phone3'] = substr($phoneNum ,6, 4);

                return;
            }
    	}

    	if($this->hasPhoneChunksButNoPhone($param)) {
            $this->phone['phone'] = $param['phone1'] . $param['phone2'] . $param['phone3'];

            return;
        }
    }

    /**
     * [hasPhoneButNoPhoneChunks description]
     * @return boolean
     */
    protected function hasPhoneButNoPhoneChunks ($param)
    {
        if($param['phone'] != ''
        && $param['phone1'] == ''
        && $param['phone2'] == ''
        && $param['phone3'] == '' ) return true;
        return false;
    }

    /**
     * [hasPhoneChunksButNoPhone description]
     * @return boolean
     */
    protected function hasPhoneChunksButNoPhone ($param)
    {
        if($param['phone'] == ''
        && $param['phone1'] != ''
        && $param['phone2'] != ''
        && $param['phone3'] != '') return true;
        return false;
    }
}
