<?php

namespace App\Http\Traits;

Trait Store {

    protected $store;

    /**
     * [storeHas description]
     */
    protected function stored()
    {
        return $this->store->all();
    }

    /**
     * [storeHas description]
     */
    protected function storeID()
    {
        return $this->store->getId();
    }

    /**
     * [storeHas description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    protected function storeHas($key)
    {
        if($this->store->has($key)) return true;
        return false;
    }

    /**
     * [storeHas description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    protected function fetchStore($key)
    {
        $data = $this->store->get($key);
        if(is_object($data)) $data = (Array) $data;
        return $data;
    }

    /**
     * [storeHas description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    protected function storing($key, $value)
    {
        return $this->store->put($key, $value);
    }

    /**
     * [storeHas description]
     * @param  [type] $data [description]
     */
    protected function storeOrReplace($data)
    {
        return $this->store->replace($data);
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function hasToken ()
    {
        if($this->storeHas('nlr.leadreactor_token')) return true;
        return false;
    }

	public function environment()
	{
        if(!$this->storeHas('path.options.general.use_production_file')) return 'production';
    	if($this->fetchStore('path.options.general.use_production_file') == 'true')  return 'production';
         return 'local';
	}

    public function pathFolder($useProduction = '')
	{
        if($useProduction) {
            if($useProduction == 'true') return 'paths';
            return 'devpaths';
        }

        if($this->environment() == 'production') return 'paths';
        return 'devpaths';
	}

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function hasFilterQuestions ()
    {
        if($this->storeHas('nlr.filter_questions')){
            if(count($this->fetchStore('nlr.filter_questions'))) return true;
        }
        return false;
    }

    /**
     * [filterQuestions description]
     * @return boolean [description]
     */
    protected function filterQuestions ()
    {
        return ($this->storeHas('nlr.filter_questions'))? $this->fetchStore('nlr.filter_questions') : [];
    }

    /**
     * [filterQuestions description]
     * @return boolean [description]
     */
    protected function filterIcons ()
    {
        return $this->fetchStore('nlr.filter_icons');
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function leadReactorUrl ()
    {
        return $this->fetchStore('nlr.leadreactor_url');
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function leadreactorToken ()
    {
        return $this->fetchStore('nlr.leadreactor_token');
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function device ()
    {
        return $this->fetchStore('nlr.device');
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function browser ()
    {
        return $this->fetchStore('nlr.browser');
    }

    /**
     * [hasToken description]
     * @return boolean [description]
     */
    protected function hasCampaigns ()
    {
        if($this->storeHas('campaigns')) return true;
        return false;
    }


    /**
     * [previousAccepted description]
     * @return [type] [description]
     */
    public function acceptingCount ()
    {
        $previous = 10;
        if($this->storeHas('nlr.previous_accepted')) $previous = $this->fetchStore('nlr.previous_accepted');
        $this->storing('nlr.previous_accepted', rand($previous, $previous + strval(date('i'))));

        return $this->fetchStore('nlr.previous_accepted');
    }

    public function pathUrl($backlashes = 1)
    {
        $path = $this->fetchStore('path.url');
        $path .= ($backlashes) ? '/' : '';
        return $path;
    }

    public function affiliateID()
    {
        return $this->fetchStore('user')['affiliate_id'];
    }

    public function pathID()
    {
        return $this->fetchStore('user')['path_id'];
    }

    public function replaceable()
    {
        return array_merge(
            $this->fetchStore('user'),
            $this->fetchStore('nlr.device'),
            $this->fetchStore('nlr.browser'),
            ['target_url' => $this->leadReactorUrl()]
        );
    }


    /**
     * Variables needed befor evaluation
     *
     * @return array
     */
    public function variablesNeededB4Eval ()
    {
        $gender = $this->fetchStore('user.gender');
        $birthdate = $this->fetchStore('user.birthdate');
        $email = $this->fetchStore('user.email');

        return [
            'gender' => (strtolower($gender) == 'f') ? 'Female' : 'Male',
            'one_letter_gender' => strtoupper($gender),
            'title' => (strtolower($gender) == 'm') ? 'Mr.' : 'Ms.',
            'age' => (int) intval(date_diff(date_create($birthdate), date_create('today'))->y),
            'lead_info' => [
                'email' => $email
            ]
        ];
    }
}
