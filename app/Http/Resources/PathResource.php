<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PathResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'slug' => $this->slug,
            'comments' => $this->comments,
            'status' => $this->status,
            'description' => $this->description,
            'date_created' => $this->created_at->diffForHumans(),
            'date_updated' => $this->updated_at->diffForHumans(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'options' => $this->options()
        ];
    }

    protected function options()
    {
        if(count($this->options)) {
            $return = [];
            foreach ($this->options as $option) {
                $value = $option['value'];
                if($option['key'] == 'files')$value =  explode(',', $value);
                if($option['key'] == 'email_recipients')$value =  explode(',', $value);

                $return[$option['section']][$option['key']] = $value;
            }

            return $return;
        }
        return;
    }
}
