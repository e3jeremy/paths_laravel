<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\User;
use App\Bug;
use Auth;
use Gravatar;

class CommentsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comments' => $this->comments,
            'bug_id' => $this->bug_id,
            'author' => $this->getUserAvatar($this->created_by),
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
            'creator' => $this->createdBy($this->created_by),
            'created_by' => $this->created_by,
            'description' => $this->getBugDescription($this->bug_id),
            //'assignto' => "to me",
        ];
    }
    private function getUserAvatar($user)
    {
        $name = User::where('name', $user)->first();
        return Gravatar::get($name->email) ? Gravatar::get($name->email) : 'me';
    }
    private function getBugDescription($id)
    {
        $bug = Bug::where('id', $id)->first();
        return $bug->description;
    }
    private function createdBy($user)
    {
        $name = User::where('name', $user)->first();
        if($name->name === Auth::user()->name) {
            return "You";
        }
        return $user;
    }
    private function assignTo($id)
    {
        $bug = Bug::where('id', $id)->first();
        $user = User::where('id',$bug->assignto)->first();
        return $user ? $user->name: "nothing";
    }
}
