<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Gravatar;
use App\Bug;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->getAvatar($this->email),
            'roles' => $this->roles()->pluck('name')->implode(' '),
            'tasks' => $this->getRelatedTasks($this->id),
            'last_loggedin' => $this->getLoggedInStatus($this->last_loggedin, $this->last_loggedout),
            'status' => $this->status,
            'date_created' => $this->created_at->diffForHumans(),
            'date_updated' => $this->updated_at->diffForHumans(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by
        ];
    }
    private function getAvatar($avatar)
    {
        return Gravatar::get($avatar);
    }
    private function getLoggedInStatus($loggedin, $loggedout)
    {
        if($loggedin && $loggedout) {
            return $loggedout->diffForHumans();
        } elseif($loggedin && is_null($loggedout)) {
            return 'online';
        } else {
            return 'user activity unknown';
        }
    }
    private function getRelatedTasks($id)
    {
        return Bug::where('user_id', $id)->get();
    }
}
