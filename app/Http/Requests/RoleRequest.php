<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * custom code to suppress error when update method is called
         */
        return [
            //'name' => 'required|unique:roles|max:255,'.$id,
            'name' => 'required',
            Rule::unique('roles')->ignore($this->get('id')),
            'permissions' => 'required',
        ];
    }
}
