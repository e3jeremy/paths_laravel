<?php

namespace  App\Http\Services;

use scssc;

use Illuminate\Filesystem\Filesystem;

class PathStyle
{
    protected $blades = [
        'campaign',
        'landing',
        'question',
        'registration'
    ];

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [compile description]
     * @param  [type] $scssFolder  [description]
     * @param  [type] $cssFolder   [description]
     * @param  string $formatStyle [description]
     * @return [type]              [description]
     */
    public function compile($scssFolder, $cssFolder, $formatStyle = 'scss_formatter_compressed')
    {
        $compiler = new \scssc();

        $compiler->setImportPaths($scssFolder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $compiler->setFormatter($formatStyle);
        // get all .scss files from scss folder
        $filelist = glob($scssFolder . "*.scss");

        // step through all .scss files in that folder
        foreach ($filelist as $filePath) {
            // get path elements from that file
            $filePathElements = pathinfo($filePath);
            // get file's name without extension
            $fileName = $filePathElements['filename'];
            // Disregard file
            if (substr($fileName, 0, 1) === '_') continue;
            // get .scss's content, put it into $stringSass
            $stringSass = file_get_contents($scssFolder . $fileName . ".scss");
            // compile this SASS code to CSS
            try {
                $stringCss = $compiler->compile($stringSass);

                // write CSS into file with the same filename, but .css extension
                file_put_contents($cssFolder . $fileName . ".css", $stringCss);

                return [
                    'error' => false
                ];
            }
            catch (\Exception $error) {
                return [
                    'error' => true,
                    'msg' => $error->getMessage()
                ];
            }
        }
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {
        $resourcePath = $this->getResourcePath($slug);
        $publicPath = $this->getPublicPath();

        $this->createIntermediateFolders($resourcePath);
        $this->createIntermediateFolders($publicPath);

        $stub = $this->getStubAndReplace('#3097D1', 'default');

        $this->file->put($resourcePath . '/' . $slug . '.min.scss', $stub);
        $this->file->put($resourcePath . '/_editor.scss', '');
        $this->file->put($resourcePath . '/_backup.scss', '');
        $this->file->put($resourcePath . '/_temp.scss', '');

        $this->compile($resourcePath . '/', $publicPath);
    }

    public function getStubAndReplace ($color, $template, $progressbar = '')
    {
        $stub = $this->file->get($this->getStub());
        $stub = str_replace('dummyColor',$color, $stub);
        $stub = str_replace('dummyTemplate', $template, $stub);
        $stub = str_replace('dummyProgressBar', $progressbar, $stub);

        return $stub;
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->deleteDirectory($this->getResourcePath($slug));
        // $this->file->deleteDirectory($this->getPublicPath($slug));
        $this->file->delete($this->getPublicPath($slug . '.min.css'));
    }

    /**
     * [size description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public function size($path)
    {
        return $this->file->size($path);
    }



    public function updateStylesheet($lug, $color, $template, $progressbar)
    {
        // Update stylesheet for compliling
        $this->file->put(
            $this->getResourcePath($lug . '/'. $lug . '.min.scss'),
            // Replace in stub
            $this->getStubAndReplace($color, $template, $progressbar)
        );
    }

    /**
     * [backup description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function backup($slug, $color, $template, $progressbar)
    {
        $compiler = new \scssc();

        $resourcePath = $this->getResourcePath($slug);

        $editor = $this->file->get($resourcePath . '/_editor.scss');

        try {
            // $this->pathStyle->updateStylesheet(
            //     $slug,
            //     $color,
            //     $template,
            //     $progressbar
            // );

            $compiler->compile($editor);

            $this->file->put($resourcePath . '/_backup.scss', $editor);
            return [
                'error' => false,
                'content' => $editor,
                'size' =>  $this->file->size($resourcePath . '/_backup.scss')
            ];
        }
        catch (\Exception $error) {
            return [
                'error' => true,
                'msg' => $error->getMessage()
            ];
        }
    }

    /**
     * [backup description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function revert($slug)
    {
        $compiler = new \scssc();

        $resourcePath = $this->getResourcePath($slug);

        $editor = $this->file->get($resourcePath . '/_editor.scss');
        $backup = $this->file->get($resourcePath . '/_backup.scss');

        try {
            $compiler->compile($backup);

            $this->file->put($resourcePath . '/_temp.scss', $editor);
            $this->file->put($resourcePath . '/_editor.scss', $backup);
            return [
                'error' => false,
                'content' => $backup,
                'size' =>  $this->file->size($resourcePath . '/_editor.scss'),
                'temp_size' =>  $this->file->size($resourcePath . '/_temp.scss')
            ];
        }
        catch (\Exception $error) {
            return [
                'error' => true,
                'msg' => $error->getMessage()
            ];
        }
    }

    /**
     * [apply description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function applyReversion($slug)
    {
        $resourcePath = $this->getResourcePath($slug);

        $this->file->put($resourcePath . '/_temp.scss', '');

        return [
            'error' => false,
            'temp_size' => 0
        ];
    }

    /**
     * [cancelReversion description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function cancelReversion($slug)
    {
        $resourcePath = $this->getResourcePath($slug);

        $temp = $this->file->get($resourcePath . '/_temp.scss');

        $this->file->put($resourcePath . '/_temp.scss', '');
        $this->file->put($resourcePath . '/_editor.scss', $temp);

        return [
            'error' => false,
            'content' => $temp,
            'temp_size' =>  0
        ];
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getResourcePath($fileName = null)
    {

        $path = resource_path() . '/assets/sass/paths';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getPublicPath ($fileName = null)
    {

        $path = public_path() . '/css/paths';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * @param string $path
     */
    protected function createIntermediateFolders($folderPath)
    {
        if (! is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
    }

    /**
     *
     *
     * @return string
     */
    public function getStub()
    {
        $path = resource_path() . '/assets/sass/stubs/path-style.custom.stub';

        return $this->normalizePath($path);
    }
}
