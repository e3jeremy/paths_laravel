<?php

namespace  App\Http\Services;

use Illuminate\Filesystem\Filesystem;

class PathStorage
{
    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }


    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {
        $path = $this->getPath($slug);

        $this->createIntermediateFolders($path);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->deleteDirectory($this->getPath($slug));
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getPath($fileName = null)
    {
        $path = storage_path() . '/paths';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

        /**
         * @param string $path
         *
         * @return string
         */
        protected function normalizePath($path)
        {
            $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

            return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
        }

        /**
         * @param string $path
         */
        protected function createIntermediateFolders($folderPath)
        {
            if (! is_dir($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
        }
}
