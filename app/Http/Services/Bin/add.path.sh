#!/bin/bash
#

# Parameters from DashboardController.php
slug=$1
controllername=$2
uppercase_foldername=$3
lowercase_foldername=$4
base_path=$5
resource_path=$6

git add $base_path/app/Http/Controllers/$uppercase_foldername/$controllername.php

git add $resource_path/views/$lowercase_foldername/$slug

git add $base_path/routes/$lowercase_foldername/$slug.php
