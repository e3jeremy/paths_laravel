<?php

namespace  App\Http\Services;

use Illuminate\Filesystem\Filesystem;

class PathRoute
{
    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug, $controllerName, $folderName)
    {
        $path = $this->getPath($folderName, $slug . '.php');

        $this->createIntermediateFile($this->getPath($folderName));

        $stub = $this->file->get($this->getStub($this->getStubPath()));

        $stub = str_replace('dummyFolderName', ucwords($folderName), $stub);
        $stub = str_replace('dummySlug', $slug, $stub);
        $stub = str_replace('dummyControllerName', $controllerName, $stub);

        $this->file->put($path, $stub);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug, $folderName)
    {
        $this->file->delete($this->getPath($folderName, $slug . '.php'));
    }


    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getPath($folderName, $fileName = null)
    {
        $path = base_path() . '/routes/' . strtolower($folderName);

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }
    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getStubPath()
    {
        $path = base_path() . '/routes/stubs';

        return $this->normalizePath($path);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * @param string $path
     */
    protected function createIntermediateFile($folderPath)
    {
        if (! is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    protected function getStub($path)
    {
        return $path . '/path-route.custom.stub';
    }

}
