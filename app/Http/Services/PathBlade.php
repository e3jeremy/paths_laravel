<?php

namespace  App\Http\Services;

use Illuminate\Filesystem\Filesystem;

class PathBlade
{
    protected $blades = [
        'campaign',
        'landing',
        'question',
        'registration'
    ];

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug, $folderName)
    {
        $path = $this->getPath(strtolower($folderName) . '/' . $slug);

        $this->createIntermediateFolders($path);

        foreach ($this->blades as $blade) {
            $stub = $this->file->get($this->getStub($this->getPath()));

            $stub = str_replace('dummyBladeName', $blade, $stub);
            $stub = str_replace('dummySlug', $slug, $stub);

            $this->file->put($path . '/' . $blade . '.blade.php', $stub);
        }
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug, $folderName)
    {
        $this->file->deleteDirectory($this->getPath(strtolower($folderName) . '/' . $slug));
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getPath($fileName = null)
    {
        /** @var \Illuminate\View\FileViewFinder $viewFinder */
        $viewFinder = app('view.finder');

        $paths = $viewFinder->getPaths();

        // If we have more than one path configured, throw an
        // exception as this is currently not supported by
        // the package. It might be supported later on.
        if (count($paths) !== 1) {
            //throw UnsupportedException::tooManyPaths(count($paths));
        }

        $path = reset($paths);

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * @param string $path
     */
    protected function createIntermediateFolders($folderPath)
    {
        if (! is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    protected function getStub($path)
    {
        return $path . '/stubs/path.custom.stub';
    }
}
