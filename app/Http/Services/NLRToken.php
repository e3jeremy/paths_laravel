<?php

namespace  App\Http\Services;

use Session;

class NLRToken
{
    /**
     *  Mcrypt Class, since nlr is using mcrypt for authentication,
     *  we will apply also here.
     *  Although the mcrypt is deprecated on latest version of php,
     *  the warning/notice error message was contraint/concealed here.
     *  Note: Change this type of encryption to "SSL Encryption"(laravel have it's own encryption), but first update the nlr authenticcation
     *        Mcrypt wll not last long and this site will crash. :)
     *
     * @var class instance of  App\Http\Services\Helpers\Encryption
     */
    protected $phpcrypt;

    /**
     * Laravel package for Curl
     *
     * @var class instance of  App\Http\Services\Helpers\Guzzle
     */
    protected $client;

    /**
     * Encryptions settings
     */
    protected $encryptionApplied = false;
    protected $encryptionKey;
    protected $macKey;
    protected $tokenEmail;
    protected $tokenPassword;
    protected $tokenokenUrl;

    /**
     * Catch curl exceptions
     *
     * @var boolean
     */
    protected $hasError = false;

    /**
     * Instantiate and inject the needed class.
     *
     * @param HelpersEncryption $phpcrypt
     * @param HelpersGuzzle     $guzzle
     */
    public function __construct (
        Helpers\Encryption $phpcrypt,
        Helpers\Guzzle $guzzle
        )
    {
        $this->phpcrypt = $phpcrypt;
        $this->guzzle = $guzzle;
    }

    /**
     * Weither to apply encryption
     *
     * @param boolean $encryptionApplied true|false
     * @return NLRToken::class
     */
    public function setEncryptionApllied($encryptionApplied)
    {
        $this->encryptionApplied = $encryptionApplied;
        return $this;
    }

    /**
     * Set encryption key, from env.
     *
     * @param string $encryptionKey
     * @return NLRToken::class
     */
    public function setEncryptionKey($encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;
        return $this;
    }

    /**
     * Set mac key, from env.
     *
     * @param string $macKey
     * @return NLRToken::class
     */
    public function setMacKey($macKey)
    {
        $this->macKey = $macKey;
        return $this;
    }

    /**
     * Set token email for authentication, from env.
     *
     * @param string $tokenEmail
     * @return NLRToken::class
     */
    public function setTokenEmail($tokenEmail)
    {
        $this->tokenEmail = $tokenEmail;
        return $this;
    }

    /**
     * Set token password for authentication, from env.
     *
     * @param string $tokenPassword
     * @return NLRToken::class
     */
    public function setTokenPassword($tokenPassword)
    {
        $this->tokenPassword = $tokenPassword;
        return $this;
    }

    /**
     * Set token url, for api call, from env.
     *
     * @param string $tokenokenUrl
     * @return NLRToken::class
     */
    public function setTokenUrl($tokenokenUrl)
    {
        $this->tokenokenUrl = $tokenokenUrl . 'getMyToken';
        return $this;
    }

    /**
     * Check if curl throws error.
     *
     * @return boolean
     */
    public function hasError()
    {
        return $this->hasError;
    }

    /**
     * The exception message.
     *
     * @return string
     */
    public function error()
    {
        return $this->guzzle->error();
    }

    /**
     * The process of accessing the token.
     *
     * @return void
     */
    public function getToken ()
    {
        $username = $this->tokenEmail;
        $password = $this->tokenPassword;

    	if($this->encryptionApplied) {
    		$this->phpcrypt->validate($this->encryptionKey, $this->macKey);

    		$username = $this->phpcrypt->encrypt($username);
    		$password = $this->phpcrypt->encrypt($password);
    	}

        $this->guzzle->setClientUrl($this->tokenokenUrl);
        $this->guzzle->setHeaders([
                'useremail' => $username,
        	    'userpassword' => $password
            ]);

        if($this->guzzle->request()) {
            $content = $this->guzzle->responseContent();

            $this->hasError = false;
            return $content->token;
        }

        $this->hasError = true;
        return;

    }
}
