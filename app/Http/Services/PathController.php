<?php

namespace  App\Http\Services;

use Illuminate\Support\Str;

use Illuminate\Filesystem\Filesystem;

class PathController
{

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug           [description]
     * @param  [type] $controllerName [description]
     * @param  [type] $folderName     [description]
     * @return [type]                 [description]
     */
    public function create ($slug, $controllerName, $folderName)
    {
        $path = $this->getPath($folderName, $controllerName);

        $this->createIntermediateFolder($this->getPath($folderName));

        $stub = $this->file->get($this->getStub());

        // // Parse input name.
        $name = str_replace('Controller', '', $controllerName);

        // Create replacements
        $viewPathName = strtolower($folderName) . '.' . strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', str_replace('\\', '.', $name)));

        // Apply replacements
        $stub = str_replace('DummyFolderName', ucwords($folderName), $stub);
        $stub = str_replace('DummyClass', $controllerName, $stub);
        $stub = str_replace('DummyViewPath', $viewPathName, $stub);
        $stub = str_replace('DummySlug', $slug, $stub);

        $this->file->put($path, $stub);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($controllerName, $folderName)
    {
        $this->file->delete($this->getPath($folderName, $controllerName));
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getPath($folderName, $fileName = null)
    {
        $path = base_path() . '/app/Http/Controllers/' . ucwords($folderName);

        if($fileName) $fileName .= '.php';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * @param string $path
     */
    protected function createIntermediateFolder($folderPath)
    {
        if (! is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    protected function getStub()
    {
        return base_path() . '/app/Http/Controllers/Stubs/Controller.custom.stub';
    }

}
