<?php

namespace  App\Http\Services;

use Illuminate\Http\Request;

class Storage
{

    public function __construct(\Illuminate\Filesystem\FilesystemManager $storage)
    {
        $this->storage =$storage;
    }

    public function StoreImage(
        Request $request,
        $imgName,
        $imgKey,
        $data = []
        )
    {
        if ($request->hasFile($imgKey)) {
            $this->storage->disk('paths')->putFileAs($request->get('slug'), $request->{$imgKey}, $imgName);
            $data[$imgKey] = '/path-image/' . $request->get('slug') . '/' . $imgName;
        }

        return $data;
    }

    public function exists($image)
    {
        if($this->storage->disk('paths')->exists($image)) return true;
        return false;
    }

    public function getImage($image)
    {
        if($this->exists($image)) return $this->storage->disk('paths')->get($image);
        return '';
    }
}
