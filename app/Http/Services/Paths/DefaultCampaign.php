<?php

namespace  App\Http\Services\Paths;

use Illuminate\Http\Request;

class DefaultCampaign
{
    /**
     * [protected description]
     * @var [type]
     */
    public $options;

    /**
     * Default variables
     *
     */
    protected $campaignLinks;
    protected $params = [];
    protected $campaigns = [];
    protected $creatives = [];

    /**
     * Instantiatte
     */
    public function __construct (
        Options $options
        )
    {
        $this->options = $options;
    }

    /**
     * Fetch parameters
     *
     * @return array
     */
    public function params()
    {
        return $this->params;
    }

    /**
     * Setting of parameters
     *
     */
    public function setParams($params)
    {
        $this->params['campaign_url'] = $params['path_url'] . 'campaign/';
        $this->params['current_stack'] = $params['current_stack'];
        $this->params['next_stack'] = $params['current_stack'] + 1;
        $this->params['stack_count'] = $params['stack_count'];
    }

    /**
     * Add to parameters
     *
     * @param array $data
     */
    public function addParams($data = '')
    {
        $this->params = array_merge($this->params, $data);
    }

    /**
     * Set the list ofcampaign ids for the current page
     *
     * @param array $campaigns
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;
    }

    /**
     * Set the list of $creatives
     *
     * @param array $creatives
     */
    public function setCreatives($creatives)
    {
        $this->creatives = $creatives;
    }

    /**
     * Fetch camapign links for api call
     *
     * @return array
     */
    public function campaignLinks()
    {
        return $this->campaignLinks;
    }

    /**
     *  Generate campaign campaignLinks
     *
     */
    public function generateCampaignLinks()
    {
        // Lists of ids to url string
        $this->campaignLinks = http_build_query(['campaigns' => $this->campaigns]);

        // look for creatives related to lists of ids
        $creatives = array();
        if($this->creatives) {
			foreach($this->campaigns as $campaignID) {
				if(array_key_exists($campaignID, $this->creatives)) $creatives[$campaignID] = $this->creatives[$campaignID];
			}
		}

        // Add the creatives if have one...
        if(count($creatives) > 0) $this->campaignLinks .= '&'.http_build_query(['creatives' => $creatives]);
    }
}
