<?php

namespace  App\Http\Services\Paths;

use Illuminate\Http\Request;

use hisorange\BrowserDetect\Parser;

class DefaultLanding
{
    /**
     * [protected description]
     * @var [type]
     */
    public $options;

    /**
     * Data needed for blade
     * @var array
     */
    protected $params = [];

    /**
     * Default data for blade
     * @var array
     */
    protected $defaults = [
        'affiliate_id' => 1,
    	'offer_id' => null,
    	'campaign_id' => null,
    	'first_name' => '',
    	'last_name' => '',
        'birthdate' => '',
    	'dobmonth' => '',
    	'dobday' => '',
    	'dobyear' => '',
    	'state' => '',
    	'city' => '',
    	'zip' => '',
    	'email' => '',
    	'gender' => '',
    	'ethnicity' => '',
    	'address' => '',
    	'phone' => '',
    	'phone1' => '',
    	'phone2' => '',
    	'phone3' => '',
    	's1' => '',
    	's2' => '',
    	's3' => '',
    	's4' => '',
    	's5' => '',
        'view' => '',
        'source_url' => '',
        'ip' => '',
        'image' => null,
        'user_agent' => '',
        'agree' => 'off',
        'submit' => '',
        'filter_questions' => []

    ];

    /**
     * Instantiate and inject needed class
     *
     * @param hisorange\BrowserDetect\Parser $browser
     */
    public function __construct (
        Options $options
        )
    {
        $this->options = $options;
    }

    /**
     * Get parameters
     *
     * @return array
     */
    public function params()
    {
        return $this->params;
    }

    /**
     * [setParams description]
     * @param [type] $inputs
     */
    public function setParams ($data)
    {
        list($inputs, $device, $browser, $fullUrl, $userIP) = $data;

        $inputs['ip'] = $userIP;
        $inputs['source_url'] = $fullUrl;
        $inputs['user_agent'] = $browser['user_agent'];

        $this->updateDefaultParams($inputs);

        $this->addView2Params($device);
    }

    /**
     * Add $params
     *
     * @param array $data
     */
    public function addParams($data = '')
    {
        $this->params = array_merge($this->params, $data);
    }

    /**
     * Params should only contains what default have.
     *
     * @param  array $inputs
     */
    protected function updateDefaultParams ($inputs)
    {
        if(is_array($inputs)) {
            foreach ($inputs as $key => $value) {
                if(array_key_exists($key, $this->defaults)) $this->defaults[$key] = $value;
            }
        }

        $this->params = $this->defaults;
    }

    /**
     * ad view as parameters
     *
     * @param array $device
     */
    protected function addView2Params ($device = [])
    {
    	$this->params['view'] = 1;
        if(count($device)) {
            if($device['isTablet']) $this->params['view'] = 3;
            if($device['isMobile']) $this->params['view'] = 2;
        }
    }
}
