<?php

namespace  App\Http\Services\Paths;

use App\Option;

class Options
{

    /**
     * Model
     *
     * @var App\Option
     */
    protected $option;

    /**
     * Container for path options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Id of path
     *
     * @var integer
     */
    protected $pathID = 1;

    /**
     * Default options
     *
     * @var array
     */
    protected $defaultOptions = [];

    /**
    * Instantiate
    *
    * @param App\Option $option
    */
    public function __construct(
        \Illuminate\Filesystem\Filesystem $file,
        Option $option
        )
    {
        $this->file = $file;
        $this->option = $option;
    }

    /**
     * Set the path id for query parameter
     * @param integer $pathID
     */
    public function setPathID($pathID = 1)
    {
        if($pathID) $this->pathID = $pathID;
    }

    /**
     * Get the saved options of path
     *
     * @return array
     */
    public function getSavedOptions()
    {
        $this->options = $this->option->find($this->pathID);
    }

    /**
     * Getch options
     *
     * @return array
     */
    public function get()
    {
        return $this->options;
    }

    public function saveInputs ($pathID, $options, $included = [])
    {
        foreach ($options as  $key => $value) {
            if($key == 'section' || $key == 'slug') continue;

            if(count($included)) if(!in_array($key, $included)) continue;

            if($value == null  || $value == '' || $value == 'undefined') {
                $this->option->where('key', $key)->where('path_id', $pathID)->delete();
                continue;
            }

            if(is_array($value)) $value = serialize($value);

            $this->option->updateOrCreate(
                [
                    'key' => $key,
                    'path_id' => $pathID
                ],
                [
                    'path_id' => $pathID,
                    'key' => $key,
                    'value' => $value,
                    'section' => $options['section']
                ]
            );
        }
    }

    public function remove($pathID, $key)
    {
        return $this->option->where('path_id', $pathID)->where('key', $key)->delete();
    }

    /**
     * Setting of options as arguments then replace to default options
     *
     */
    public function set($options = [])
    {
        $template = 'default';
        if(is_array($options)) {
            if(array_key_exists('themes', $options)) {
                if(array_key_exists('template', $options['themes'])) $template = $options['themes']['template'];
            }
        } else {
            $options = [];
        }

        $path = $this->getPath('templates/' . $template . '/options.ini');

        if($this->file->exists($path)) {
                $this->defaultOptions = $this->parseIniFile($path, true);
        }

        $this->options = array_replace_recursive($this->defaultOptions, $options);

        $this->unserialized();
    }

    /**
     * Values to array
     *
     */
    protected function unserialized () {
        if(!array_key_exists('trackers', $this->options)) return;

        if($this->options['trackers']['files']) $this->options['trackers']['files'] = explode(',', $this->options['trackers']['files']);
    }


    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function getPath($fileName = null)
    {
        /** @var \Illuminate\View\FileViewFinder $viewFinder */
        $viewFinder = app('view.finder');

        $paths = $viewFinder->getPaths();

        // If we have more than one path configured, throw an
        // exception as this is currently not supported by
        // the package. It might be supported later on.
        if (count($paths) !== 1) {
            //throw UnsupportedException::tooManyPaths(count($paths));
        }

        $path = reset($paths);

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    protected function parseIniFile($file, $processSections = 0, $scannerMode = INI_SCANNER_NORMAL) {
        $explodeStr = '.';
        $escapeChar = "'";
        // load ini file the normal way
        $data = parse_ini_file($file, $processSections, $scannerMode);

        foreach ($data as $sectionKey => $section) {
            // // // loop inside the section
            foreach ($section as $key => $value) {
                if (strpos($key, $explodeStr)) {
                    if (substr($key, 0, 1) !== $escapeChar) {
                        // key has a dot. Explode on it, then parse each subkeys
                        // and set value at the right place thanks to references
                        $subKeys = explode($explodeStr, $key);
                        if(end($subKeys) == 'name') {
                            $indx = $value;
                            $data[$sectionKey][$value] = '';
                        }
                        if(end($subKeys) == 'default') $data[$sectionKey][$indx] = $value;

                        // unset the dotted key, we don't need it anymore
                        unset($data[$sectionKey][$key]);
                    }
                }
            }
        }
        return $data;
    }
}
