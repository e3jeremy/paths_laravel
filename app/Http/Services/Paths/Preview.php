<?php

namespace  App\Http\Services\Paths;


class Preview extends Options
{
    protected $params = [];

    /**
     * [__construct description]
     */
    public function __construct (
        Options $options,
        \Illuminate\Filesystem\Filesystem $file
        )
    {
        $this->options = $options;
        $this->file = $file;
    }

    /**
     * [params description]
     * @return [type] [description]
     */
    public function params()
    {
        return $this->params;
    }

    /**
     * [setParams description]
     * @param [type] $params [description]
     */
    public function setParams ($params)
    {

        $this->options->set([
            'general' =>[
                'path_title' => ucwords($params['template']) . ' Template'
            ],
            'themes' =>[
                'template' => $params['template']
            ],
            'header' =>[
                'header_title' =>  ucwords($params['template']) . ' Template ....',
            ],
            'style' => [
                'version' => 'v' . time()
            ],
            'feedback' => [
                'show_feedback' => false
            ]
        ]);

        $this->params['options'] = $this->options->get();
        $this->params['is_stack_page'] = false;
        $this->params['progress'] = '50%';
        $this->params['slug'] = $params['template'];
        $this->params['current_page'] = '';
        $this->params['token_error'] = '';


    }
}
