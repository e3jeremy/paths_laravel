<?php

namespace  App\Http\Services\Helpers;

class Guzzle
{
    /**
     * Laravel package for Curl
     *
     * @var class instance of  GuzzleHttp\Client
     */
    protected $client;

    /**
     * Target url for api call.
     *
     * @var string
     */
    protected $url;

    /**
     * The response of api call
     *
     * @var any json|xml|html
     */
    protected $response;

    /**
     * The exception message
     *
     * @var string
     */
    protected $error;
    protected $hasError = true;

    /**
     * Body for api call
     *
     * @var array
     */
    protected $options = [];
    protected $headers = [];
    protected $query = [];
    protected $formParams = [];

    /**
     * Instantiate and inject the needed class.
     *
     * @param GuzzleHttpClient $client
     */
    public function __construct(
        \GuzzleHttp\Client $client
        )
    {
        $this->client = $client;
    }

    /**
     * client url for api call
     *
     * @param string $url
     */
    public function setClientUrl($url)
    {
        $this->url = $url;
    }


    /**
     * Headers for api call
     *
     * @param array $headers
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * Headers for api call
     *
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * Query for api call
     *
     * @param array $headers
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * Form params for api call
     *
     * @param array $headers
     */
    public function setFormParams($params)
    {
        $this->formParams = $params;
    }

    public function hasError()
    {
        return $this->hasError;
    }

    /**
     * The process of api call.
     * Use http methods for RESTful services
     *
     * @param  string $method POST|GET|PUT|PATCH|DELETE
     * @return bolean If the request is successfull
     */
    public function request($method = 'POST')
    {
        try {
            $this->response = $this->client->request($method, $this->url, $this->options($method));
            $this->hasError = false;
            return true;
        }
        catch(\Exception $error)
        {
            $this->hasError = true;
            $this->error = $error->getMessage();
            $this->hasError = true;
            return false;
        }
    }

    /**
     * Generate the options
     *
     * @param string $method
     * @return array
     */
    protected function options ($method = 'POST')
    {
        if($this->headers) $this->options['headers'] = $this->headers;
        if(strtolower($method) == 'get' && $this->query) $this->options['query'] = $this->query;
        if(strtolower($method) == 'get' && !$this->query && $this->formParams) $this->options['form_params'] = $this->formParams;
        if(strtolower($method) == 'post' && $this->formParams) $this->options['form_params'] = $this->formParams;

        return $this->options;
    }

    /**
     * The error message
     *
     * @return string
     */
    public function error()
    {
        return $this->error;
    }

    /**
     * Get the response status code
     *
     * @return string
     */
    public function responseStatusCode()
    {
        if($this->hasError) return;

        return $this->response->getStatusCode();
    }

    /**
     * Get the response header line content-type|*
     *
     * @param  string $headerLine
     * @return string
     */
    public function responseHeaderLine($headerLine)
    {
        if($this->hasError) return;

        return $this->response->getHeaderLine($headerLine);
    }

    /**
     * how to parse content by content type.
     *
     * @param  boolean $parse weither to parse or not
     * @return json|xml|html
     */
    public function responseContent($parse = 1)
    {
        if($this->hasError) return;

        if($parse) {
            if($this->responseHeaderLine('content-type') == 'application/json') return $this->parseJson($this->response);
            if($this->responseHeaderLine('content-type') == 'application/xml') return $this->parseXmlResponse($this->response);
            if($this->responseHeaderLine('content-type') == 'text/xml') return $this->parseXmlResponse($this->response);
            if($this->responseHeaderLine('content-type') == 'text/html; charset=UTF-8') return $this->response->getBody()->getContents();
        }

        return $this->response->getBody()->getContents();
    }

    /**
     * Process on parsing json response
     *
     * @param  json $response
     * @return array
     */
    protected function parseJson($response)
    {
        return  json_decode($response->getBody()->getContents());
    }

    /**
     * Process on parsing xml response
     *
     * @param  xml $response
     * @return array
     */
    protected function parseXmlResponse($response)
    {
        return json_decode(json_encode(simplexml_load_string($response->getBody()->getContents())));
    }
}
