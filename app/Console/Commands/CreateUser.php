<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Mail\UserCreated;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roles = Role::pluck('name')->toArray();
        $rolesString =implode(",", $roles);
        $users = User::pluck('name')->toArray();
        $emails = User::pluck('email')->toArray();

        $newUserRole = $this->ask("Please select role " . $rolesString);
        if(!in_array($newUserRole, $roles)) {
            return $this->error('Roles should be ' . '[' .$rolesString .']');
        }

        $newUserName = $this->ask("Enter Username");
        if(in_array($newUserName, $users)) {
            return $this->error($newUserName . " already exist!");
        }

        $newUserEmail = $this->ask("Enter Email");

        if(in_array($newUserEmail, $emails)) {
            return $this->error($newUserEmail . ' Already Exists');
        }
        if (!filter_var($newUserEmail)) {
            return $this->error($newUserEmail .' is considered not valid email baby');
        }
        $password = $this->ask('Please enter password, Password [minimum length is 6]');
        if(strlen($password) < 6) {
            return $this->error("Password required minimum length is 6");
        }

        $user = new User();
        $user->name = $newUserName;
        $user->email = $newUserEmail;
        $user->password = bcrypt($password);
        $user->status = 1;
        $user->created_by = 'System';
        $user->updated_by = 'System';
        $user->save();

        $role = $newUserRole;
        $roleR = Role::where('name', $role)->firstOrFail();
        $user->assignRole($roleR);

        $this->info("Username " .$newUserName . ' with email' . $newUserEmail . ' with Role ' . $newUserRole . ' is created');
    }
}
