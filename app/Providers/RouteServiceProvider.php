<?php

namespace App\Providers;

use App\Path;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
	/**
	* Traits
	*/
	use \App\Http\Traits\Store;

	/**
	* This namespace is applied to your controller routes.
	*
	* In addition, it is set as the URL generator's root namespace.
	*
	* @var string
	*/
	protected $namespace = 'App\Http\Controllers';

	/**
	* Define your route model bindings, pattern filters, etc.
	*
	* @return void
	*/
	public function boot()
	{
		//

		parent::boot();
	}

	/**
	* Define the routes for the application.
	*
	* @return void
	*/
	public function map(
		Store $store
		)
	{
		$this->store = $store;

		$this->mapApiRoutes();

		$this->mapWebRoutes();

		$this->mapPathsRoutes();

		//
	}

	/**
	* Define the "web" routes for the application.
	*
	* These routes all receive session state, CSRF protection, etc.
	*
	* @return void
	*/
	protected function mapWebRoutes()
	{
		Route::middleware('web')
		->namespace($this->namespace)
		->group(base_path('routes/web.php'));
	}

	/**
	* Define the "api" routes for the application.
	*
	* These routes are typically stateless.
	*
	* @return void
	*/
	protected function mapApiRoutes()
	{
		Route::prefix('api')
		->middleware('auth:api')
		//->middleware('api')
		->namespace($this->namespace)
		->group(base_path('routes/api.php'));
	}

	/**
	* Define the "api" routes for the application.
	*
	* These routes are typically stateless.
	*
	* @return void
	*/
	protected function mapPathsRoutes()
	{
		$egments = $this->app->request->segments();

		if(count($egments) <= 1) return;

		$folderName = 'paths';

        if(config('app.env') != 'production') {
			if($path = Path::select('id', 'status')->where('slug', $egments[1])->with(['options' => function ($query) {
				return $query->select('path_id', 'key', 'value')
							 ->where('section', 'general')
							 ->where('key', 'use_production_file');
			}])->first()) {
				$options = $path->options->toArray();

				$folderName = $this->pathFolder((count($options)) ? $options[0]['value']: 'false');
			}
		}

		if($egments[0] != 'paths'
		|| $egments[1] == 'default'
		|| !file_exists(base_path('routes/' . $folderName . '/' . $egments[1] . '.php'))
		) return;

		Route::prefix('paths')
			 ->middleware('web')
			 ->namespace($this->namespace)
			 ->group(base_path('routes/' . $folderName . '/' . $egments[1] . '.php'));
	}
}
