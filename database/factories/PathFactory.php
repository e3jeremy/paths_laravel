<?php

use Faker\Generator as Faker;

$factory->define(App\Path::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'url' => $faker->url,
        'comments' => $faker->sentence,
        'status' => 1,
        'slug' => $faker->slug,
        'created_by' => $faker->name,
        'updated_by'=> $faker->name,
        'created_at' => '2018-09-08',
        'updated_at' => '2018-09-09'
    ];
});
