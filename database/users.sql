/*
Navicat SQLite Data Transfer

Source Server         : sqlite
Source Server Version : 30623
Source Host           : localhost:0

Target Server Type    : SQLite
Target Server Version : 30623
File Encoding         : 65001

Date: 2018-02-19 13:54:50
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."users"
-- ----------------------------
DROP TABLE "main"."users";
CREATE TABLE "users" ("id" integer not null primary key autoincrement, "name" varchar not null, "email" varchar not null, "avatar" text null, "role" varchar null, "last_loggedin" datetime null, "status" tinyint(1) not null default '1', "created_by" varchar null, "updated_by" varchar null, "password" varchar not null, "remember_token" varchar null, "created_at" datetime null, "updated_at" datetime null);

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO ""."users" VALUES (1, 'ian', 'ianrussel@engageiq.com', null, null, '2018-02-19 05:35:43', 1, null, null, '$2y$10$OcEkuptJ494Cvkxuy2dmyOr1gI./KDeam8M8pgi9FsTvSaKeIEBKe', 'NgynJAIbQHzBwAea0UD6ejYI55cO28qP6KIl8X7XwozYwjHcRldi7wcWUYsZ', '2018-02-15 04:28:13', '2018-02-19 05:35:43');
INSERT INTO ""."users" VALUES (9, 'ee', 'eqw@yahoo.com', null, null, null, 0, null, null, '$2y$10$aefZX/2yoMbGAgJoB8kQLucQY3dEg/z6axZPOBuE/DwptnqxJfNH6', null, '2018-02-19 01:57:45', '2018-02-19 05:35:16');
INSERT INTO ""."users" VALUES (10, 'fsdf', 'dff@yahoo.com', null, null, null, 1, null, null, '$2y$10$HCk3/ag3eGeEXYklD4JznOCbif7drl9omh387dwKh9slOaDZZm.gq', null, '2018-02-19 02:07:53', '2018-02-19 05:35:52');
INSERT INTO ""."users" VALUES (11, 'fsdf', 'dfrrf@yahoo.com', null, null, null, 1, null, null, '$2y$10$MMicWm3iLELZaqNEveDXVeZdfizvPTxGGInROz7Nif49eDMDgy45K', null, '2018-02-19 02:08:42', '2018-02-19 02:08:42');
INSERT INTO ""."users" VALUES (12, 'fsdfsss', 'dfrrfrr@yahoo.com', null, null, null, 1, null, null, '$2y$10$wfO7FykdqYFTUNW3j2AlFOW6LJSYpbrNzE3mInaJo8t6zNeUUt2NS', null, '2018-02-19 02:09:20', '2018-02-19 02:09:20');
INSERT INTO ""."users" VALUES (13, 'dsfsf', 'sdfsdfsf@gmail.com', null, null, null, 1, null, null, '$2y$10$Oha41LDk4UlTItJtb/ky3ugbrZ1Dj0e5qYwrSwn3QuP6mB.H9.oia', null, '2018-02-19 05:28:35', '2018-02-19 05:28:35');
INSERT INTO ""."users" VALUES (14, 'erwerw', 'rwerewrwer@yahoo.com', null, null, null, 1, null, null, '$2y$10$kA1Uc0IHm9eq6uD4vZNyE.PZlap1H8FHI7pQXvOOSqEu0CDT6BiPy', null, '2018-02-19 05:32:56', '2018-02-19 05:32:56');
