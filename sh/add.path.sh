#!/bin/sh

# Parameters from DashboardController.php
slug=$1
base_path=$2
controller_foldername=$3
controllername=$4
resource_path=$5
foldername=$6
action=$7
pathname=$8
username=$9

# Add controller class
git add $base_path/app/Http/Controllers/$controller_foldername/$controllername.php

# Add view folder - blades
git add $resource_path/views/$foldername/$slug

# Add route file
#git add $base_path/routes/$folderName/$slug.php
git add $base_path/routes/$folderName

# Commit description
git commit -m "$action of path name: $pathname"

#cd ~/automatedpath.paidforresearch.com

#eval $(ssh-agent -s)

#ssh-add ~/.ssh/automatedpath

# Push to repo
git push --repo https://$username@bitbucket.org/engageiq-ianrussel/paths_laravel.git
